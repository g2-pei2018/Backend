## This is the backend that 
he chosen dependency manager
* `mix deps.get` (to install all the required dependencies)
* `mix ecto.create` (to create your database)
* `mix ecto.migrate` (to migrate your database)
* `mix phx.server` (runs the default project within mix.exs)
* `mix run priv/repo/seeds.exs` (to fill your database with some mock data, generated randomly with `faker` module)
* `mix phx.gen.schema Post posts title:string body:string` (generates a schema/model, which will contain the following, 
  * a schema file in lib/backend_web/schema/post.ex, with a posts table, as it follows,
    ```
      schema "posts" do
        field :title, :string
        field :body, :string
        timestamps()
      end
    ```
  * a migration file for the repository in priv/repo/migrations, `20171115133827_create_users.exs`, where `20171115133827` is the datetime upon schema generation.

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.

Ready to run in production? Please [check our deployment guides](http://www.phoenixframework.org/docs/deployment).

## How to populate

* `mix run priv/repo/newEv.exs` (Insert EVs)
* `mix run priv/repo/seedsUsersEVs.exs` (Set EVs for a User in the N:N relation)
* `mix run priv/repo/seedsUsersEVsStats.exs` (Get Record from the N:N relation and update Statistics)

## How to use graphiql engine

* `mix phx.server` (runs the default project within mix.exs)

Now you can visit [`localhost:4000/graphiql`](http://localhost:4000/graphiql) from your browser.
Firstly, you need to login in order to get your JWT token, as it follows,
*   ```
    mutation UserLogin {
    login(email: "ryan@ryan.com", password: "foobar") {
        token
    }
    }
    ```
Secondly, you can query any schema after adding Header as it follows,
*   Add
    * Header Name: Authorization 
    * Header Value: Bearer token, where token is the value of the above string token.

![Image 1](outputs/graphiql-backend-updated.png?raw=true "Title")

## Our project structure

To facilitate member integration the project follows the following structure

```
+---------------------------------------------------------------------------------------------+
|                     Structure of The Project Tree                                           |
|---------------------------------------------------------------------------------------------|
|                                                                                             |
|    BACKEND/                                                                                 |
|    +------>mix.exs/                                                                         |
|    +------>config/                                                                          |
|            +-->config.exs/                                                                  |
|            +-->dev.exs/                                                                     |
|            +-->dev.secret.exs/                                                              |
|            +-->prod.exs/                                                                    |
|            +-->prod.secret.exs/                                                             |
|    +------>priv/                                                                            |
|            +-->repo/                                                                        |
|            |   +--------->seeds.exs/                                                        |
|            |   +--------->migrations/                                                       |
|            |   |                +-------->20171115133827_create_users.exs                   |
|            |   |                +-------->20171115133828_create_posts.exs                   |
|            |   |                +-------->20171115205459_add_password_hash_to_users.exs     |
|    +------>lib/                                                                             |
|            +-->backend/                                                                     |
|            |   +--------->guardian_serialiazer.ex                                           |
|            +-> backend_web/                                                                 |
|            |   +------>plugs/                                                               |
|            |   |                +-------->context.ex                                        |
|            |   +------>resolvers/                                                           |
|            |   |                +-------->post_resolver.ex                                  |
|            |   |                +-------->user_resolver.ex                                  |
|            |   +------>schema/                                                              |
|            |   |                +-------->post.ex                                           |
|            |   |                +-------->user.ex                                           |
|            |   |                +-------->types.ex                                          |
|            |   |                +-------->session.ex                                        |
|            |   +------>router.ex                                                            |
|            |   +------>schema.ex                                                            |
|                                                                                             |
+---------------------------------------------------------------------------------------------+
```
The above presented structure follows very simple yet important rules with the intent of maintaing and organuizing code.

Our `mix.exs` file contains the project configuration, like what modules and dependencies will be available for use.

Our `config` folder contains five main important files which will be explained in detail bellow:

* ## config.exs
    This file contains all the required configurations, including Guardian lib which allows JWT Authentication.

* ## dev.exs
    This file contains all the required configurations regarding developer endpoint, including an import with all developer database configurations, which due to privacy issues are defined in `dev.secret.exs` and ignored when `git push`(.gitignore has a rule to prevent the upload of that file)
    It is also in this file that one can configure the security options of the web application, this entails guarnishing the application with a valid key + certificates, luckily the boiler plate code already provides you with the necessary information to do so, the production version of the application will have an heightened difficulty but it is also explained on prod.exs.

* ## dev.secret.exs
    This file contains all developer database related configurations, such as, username and password for accessing the local database. It also defines the database engine, which in this case is PostgreSQL.
  
* ## prod.exs
    This file contains all the required configurations regarding production endpoint, including an import with all production database configurations, which due to privacy issues are defined in `prod.secret.exs` and ignored when `git push`(.gitignore has a rule to prevent the upload of that file)

* ## prod.dev.exs
    This file contains all production database related configurations, such as, username and password for accessing the local database. It also defines the database engine, which in this case is PostgreSQL.

Our `priv/repo` folder contains two main important folders/files which will be explained in detail bellow:

* ## seeds.exs
    This file contains mock data, which will allow better testing, i.e, this file will populate the created database. To do so you need to run `mix run priv/repo/seeds.exs` as explained earlier.

* ## migrations/
    This folder contains all migrations files, i.e., after creating a schema/model, you need a migration file so `mix ecto.migrate` can create/migrate new schemas table to the database. Each time you modify any schema, like post.ex, you need to run the aforementioned command. Make sure that migration's file prefix is a datetime, i.e., `20171115133827` refers to 2017-11-15 at 13:38:27. That command runs sequencially ordered by datetime, so later modifications needs to have later datetime, if not, it will crash/do nothing. This file has the table which will be created or updated, is equivalent to create/update table in MySQL.

Our `lib/` folder contains two main important folders/files which will be explained in detail bellow:

* ## backend/
    Our `backend/` folder contains one main important folders/files which will be explained in detail bellow:

      * ## guardian_serialiazer.ex 
          This file contains the functions that gets a user (schema) from its JWT token and sets a user for a certain JWT token. For this project this file will not have more changes.

* ## backend_web/
    Our `backend_web/` folder contains five main important folders/files which will be explained in detail bellow:
    
      * ## plugs/  
          This folder contains one main important folders/files which will be explained in detail bellow:
            
              * ## context.ex
                        This file contains the Guardian Plug, that is used to invoke Guardian lib.

      * ## resolvers/   
          This folder contains two main important folders/files which will be explained in detail bellow:
            
              * ## post_resolver.ex
                    This file contains all functions that will be called in `schema.ex`, to produce the response to graphql request from frontend or mobile, regarding table post, i.e, all Post CRUD operations are defined here.

              * ## user_resolver.ex
                    This file contains all functions that will be called in `schema.ex`, to produce the response to graphql request from frontend or mobile, regarding table user, i.e, all User CRUD operations are defined here.

          For each new table, you have to create a resolver according to its content, enabling CRUD operations, i.e., all mutations or queries functions created in the frontend or mobile, like create post, etc, needs to be defined in `resolvers` folder.

      * ## schema/
          This folder contains two main important folders/files which will be explained in detail bellow:
            
              * ## post.ex
                  This file contains the schema of Post, i.e., defines a schema that matches the table Post created and migrated (present in migrations/repo/20171115133828_create_posts.exs). Anytime you want to add new relatioships or attributes regarding table Post, you need
                  to make the necessary modifications in both files. 

              * ## user.ex
                  This file contains the schema of User, i.e., defines a schema that matches the table User created and migrated (present in migrations/repo/users.exs). Anytime you want to add new relatioships or attributes regarding table User, you need
                  to make the necessary modifications in both files. 

              * ## session.ex
                  This file contains JWT authentication function, which will be called upon login/signup, hashing user passwords for comparison, i.e, all passwords are encrypted. Like `guardian_serialiazer.ex`, this file is immutable, as long as, authentication continues to be through email and password. By enabling potencial users to create accounts and login with social networks, this file may have to be revised to authenticate with those options also.

              * ## types.ex
                  This file contains object definition that links existing schemas to the resolvers, that will be called upon received request at /api/graphql from frontend and mobile apps. Also those definitions will be used in `schema.ex`

          For each new table, after `mix phx.gen.schema`, you have to make sure that schema was placed in this folder, and that its content matches the migration file also created. In addition, you need to create in `types.ex` that schema object. For instance if you create a new schema called comment, the expected result is, 

              * ## `20171130115428_create_comments.exs` in `priv/repo/migrations`
              * ## `comment.ex` in `backend_web/schema/`
              * ## And also the following excerpt in `backend_web/schema/types.ex`
                  ```
                    object :comment do
                        field :nameElixir, :nameDB
                    end  
                  ```

      * ## router.ex
            This file contains all routes defined for the backend server.
           
              ```
                  pipeline :graphql do
                    plug Guardian.Plug.VerifyHeader, realm: "Bearer"
                    plug Guardian.Plug.LoadResource
                    plug BackendWeb.Plugs.Context
                  end

                  scope "/api" do
                    pipe_through :graphql
                
                    forward "/", Absinthe.Plug, schema: BackendWeb.Schema
                  end
                
                  forward "/graphiql", Absinthe.Plug.GraphiQL,
                  schema: BackendWeb.Schema
              ```

              * ## `pipeline :graphql` is responsible for adding a header in each response/request with JWT (only responds if request has JWT token valid)
              * ## `/api` is the uri where frontend and mobile will send their requests.
              * ## `/graphiql` is an interactive playground like graphcool playground, that allows developers to test requests to this backend server.

            This file is also immutable so far since all required routes are defined. In the future will be necessary to add extra routes for the backend-sub_backend communications.

      * ## schema.ex
          This file contains all mutations and queries available to use in the frontend and mobile, for instance, if the frontend needs to query the graphql server (this backend), in order to get all comments, previous example, this file needs to have that query defined here as well, in order to know which resolver to invoke(and consequently produce a response).


The unmentioned files and folders will not have any changes, because they were created with `mix phx.new`, i.e., when the project was first created and are immutable for this project.

## Learn more
* Official website: http://www.phoenixframework.org/
* Guides: http://phoenixframework.org/docs/overview
* Docs: https://hexdocs.pm/phoenix
* Mailing list: http://groups.google.com/group/phoenix-talk
* Source: https://github.com/phoenixframework/phoenix
* Schemas Generation: https://hexdocs.pm/phoenix/Mix.Tasks.Phx.Gen.Schema.html
* Guardian Serializer: https://github.com/ueberauth/guardian
