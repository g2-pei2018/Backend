#!/bin/sh
if [ "$1" = "GENSCHEMA" ];then
	# Creating DB schema for WattCharger Social Network
	mix phx.gen.schema User users name:string username:string email:string password:string
	#binary for avatar picture 
	#see more at https://medium.com/@paulfedory/how-and-why-to-store-images-in-your-database-with-elixir-eb80133eb945
	mix phx.gen.schema Post posts title:string body:string upvotes:integer downvotes:integer
	# TODO fix - currently server done with graphcool has Link instead of Post. Link schema is:
	# We have to adapt frontend to use Post, i.e., define new components similar to Link but with our Post schema
	# and replace all Link related invocations for Post invocations!!!
	mix phx.gen.schema Image images imageURL:string

	mix phx.gen.schema Comment comments comment:string

	# Creating DB schema for WattCharger User Profile
	mix phx.gen.schema EV evs brand:string model:string year:integer chargingStandard:string maxCharge:integer
	mix phx.gen.schema Statistic statistics consumption:float savings:float

	# Creating DB schema for WattCharger Chargers
	mix phx.gen.schema Charger chargers standard:string powerSupply:string power:integer voltage:integer maxCurrent:integer plug:string home:integer available:integer
	# home and available will be optional and are binary numbers: 0-false 1-true

	mix phx.gen.schema Route routes nrPCN:integer nrPCR:integer
	mix phx.gen.schema Place places latitude:float longitude:float streetName:string zipCode:string country:string

	mv user.ex comment.ex image.ex ev.ex charger.ex place.ex post.ex route.ex statistic.ex ../backend_web/schema
fi
if [ "$1" = "FETCHDEPS" ];then
	mix deps.get
	mix deps.compile
fi
if [ "$1" = "SETUPDB" ];then
	mix ecto.drop
	mix ecto.create
	mix ecto.migrate
	# Create mock data in seeds.exs first
	# If seeds.exs is empty, then this command does nothing!!!
	# iex -S mix run priv/repo/seeds.exs
	mix run priv/repo/Real\ Data/seedsRealEVs.exs
	mix run priv/repo/Real\ Data/seedsRealChargers.exs
	mix run priv/repo/Real\ Data/seedsRealSocial.exs
	mix run priv/repo/Real\ Data/adminUser.ex
	mix phx.server
fi
# if [ "$1" = "TESTDB" ];then
# 	# Create mock data in seeds.exs first
# 	# If seeds.exs is empty, then this command does nothing!!!
# 	# iex -S mix run priv/repo/seeds2.exs
# fi
# TODO new - Relationships only after migrating raw tables/schemas and by hand (modify manually)
# Add relationships like this
	# Inside schema/user.ex --> User schema created on top of this script add, before timestamps(),
		# has_many :posts, BackendWeb.Schema.Post
		# has_many :comments, BackendWeb.Schema.Comment
		# has_one :images, BackendWeb.Schema.Image
	# Inside schema/image.ex --> Image schema created on top of this script add, before timestamps(),
		# belongs_to :user,BackendWeb.Schema.User
		# belongs_to :post,BackendWeb.Schema.Post
	# Inside schema/post.ex --> Post schema created on top of this script add, before timestamps(),
		# belongs_to :user,BackendWeb.Schema.User
		# has_many :comments, BackendWeb.Schema.Comment
		# has_many :images, BackendWeb.Schema.Image
 	# Inside schema/comment.ex --> Comment schema created on top of this script add, before timestamps(),
		# belongs_to :post,BackendWeb.Schema.Post
		# belongs_to :user,BackendWeb.Schema.User

# The example above shows 1:N relationships between User:Post, Post:Comment and User:Comment, 
# and has redundancy due to the latter relationship. 

# For the rest of schemas add,
	# Inside schema/EV.ex --> EV schema created on top of this script add, before timestamps(),
		# many_to_many :users, User, join_through: "evs_users"
	# Inside schema/user.ex --> User schema created on top of this script add, before timestamps(),
		# many_to_many :evs, EV, join_through: "evs_users"
# TODO possible fix - Make sure that N:N is implemented like that!!
# see more at http://blog.roundingpegs.com/an-example-of-many-to-many-associations-in-ecto-and-phoenix/
############################################################################################################
# Alternatively define 1:N so user can have multiple cars, but one car only belongs to one user.
# In this scenario, please add attribute license plate or other unique identifier.
	# Inside schema/EV.ex --> EV schema created on top of this script add, before timestamps(),
		# belongs_to :user,BackendWeb.Schema.User
	# Inside schema/user.ex --> User schema created on top of this script add, before timestamps(),
		# has_many :evs, BackendWeb.Schema.EV
############################################################################################################
	# Inside schema/EV.ex --> EV schema created on top of this script add, before timestamps(),
		# has_one :statistics, BackendWeb.Schema.Statistic
	# Inside schema/Statistic.ex --> Statistic schema created on top of this script add, before timestamps(),
		# belongs_to :ev,BackendWeb.Schema.EV
# TODO possible fix - Make sure that 1:1 relationships are defined like this.
# see more at https://hexdocs.pm/ecto/Ecto.Schema.html and at 
# https://stackoverflow.com/questions/40576042/build-has-one-relationship

# TODO new - Finally Implement relationships with Charger after "defining Map/route persistence schemas"
# as Database MySQL model placed on Google Drive shows!!!




