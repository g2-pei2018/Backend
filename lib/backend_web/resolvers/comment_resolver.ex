#require IEx
defmodule BackendWeb.Resolvers.CommentResolver do
    alias Backend.Repo
    alias BackendWeb.Schema.Comment
    alias BackendWeb.Schema.Media
    alias BackendWeb.Schema.UserCommentVote
    import Ecto.Query, only: [where: 2, from: 2]

    def user_has_voted_comment(%{id: comment_id}, %{context: %{current_user: %{id: id}}}) do

        query=from ucv in UserCommentVote, where: ucv.user_id == ^id and ucv.comment_id == ^comment_id, select: ucv.vote

        case Repo.all(query) do
            nil -> {:error, "User has not voted"}
            votes -> {:ok, votes}
        end
    end

    def user_has_voted_comment(_args, _info) do
        {:error, "Not Authorized"}
    end

    def all_comments(_args, %{context: %{current_user: %{id: id}}}) do
        case Repo.all(Comment) do
            nil -> {:error, "Comments not found"}
            comments -> {:ok, comments}
        end
    end

    def all_comments(_args, _info) do
        {:error, "Not Authorized"}
    end

    def search_comments(%{wildcard: wildcard}, %{context: %{current_user: %{id: id}}}) do

        query=from c in Comment,  where: like(c.comment, ^wildcard), select: c

        case Repo.all(query) do
            nil -> {:error, "Posts not found"}
            posts -> {:ok, posts}
        end
    end

    def search_comments(_args, _info) do
        {:error, "Not Authorized"}
    end

    def comments_from_current_user(_args, %{context: %{current_user: %{id: id}}}) do
        comments =
            Comment
            |> where(user_id: ^id)
            |> Repo.all
        case comments do
            nil -> {:error, "Comments for User id #{id} not found"}
            comments -> {:ok, comments}
        end    
    end
    
    def comments_from_current_user(_args, _info) do
        {:error, "Not Authorized"}
    end

    def comments_from_user(%{id: user_id}, %{context: %{current_user: %{id: id}}}) do
        comments =
            Comment
            |> where(user_id: ^user_id)
            |> Repo.all
        case comments do
            nil -> {:error, "Comments for User id #{id} not found"}
            comments -> {:ok, comments}
        end    
    end
    
    def comments_from_user(_args, _info) do
        {:error, "Not Authorized"}
    end

    def comments_from_post(%{id: post_id}, %{context: %{current_user: %{id: id}}}) do
        comments = 
            Comment
            |> where(post_id: ^post_id)
            |> Repo.all
        case comments do
             nil -> {:error, "Comments for Post id #{id} not found"}
             comments -> {:ok, comments}
        end
    end

    def comments_from_post(_args, _info) do
        {:error, "Not Authorized"}
    end

    def num_comments_from_post(%{id: post_id}, _info) do
        comments = 
            Comment
            |> where(post_id: ^post_id)
            |> Repo.aggregate(:count, :id)
        #IEx.pry
        number = %{ "number": comments }
        case comments do
            0 -> {:error, "0 comments"}
            _ -> {:ok, number}
        end
    end

    def get_comment_by_id(%{id: comment_id}, %{context: %{current_user: %{id: id}}}) do
        case Repo.get(Comment, comment_id) do
            nil -> {:error, "Comment id #{id} not found"}
            comment -> {:ok, comment}
        end
    end

        def get_comment_by_id(_args, _info) do
        {:error, "Not Authorized"}
    end

    def get_comment_upvotes(%{id: comment_id}, _info) do
        case Repo.get(Comment, comment_id) do
            nil -> {:error, "Comment not found"}
            comment -> {:ok, comment}
        end
    end
    
    def get_comment_downvotes(%{id: comment_id}, _info) do
        case Repo.get(Comment, comment_id) do
            nil -> {:error, "Comment not found"}
            comment -> {:ok, comment}
        end
    end

    def add_upvote_comment(%{id: comment_id}, %{context: %{current_user: %{id: id}}}) do
        args = Map.merge(%{comment_id: comment_id, user_id: id},%{vote: "upvote"})

        query = from c in Comment,
                where: c.id == ^comment_id
        Repo.transaction fn ->
            case Repo.update_all(query, inc: [upvotes: 1]) do
                nil ->
                    Repo.rollback "Comment not found"
                _ -> 
                    %UserCommentVote{}
                    |> UserCommentVote.changeset(args)
                    |> Repo.insert()
                    |> case do
                        {:ok, result} -> result
                        {:error, _} ->
                            Repo.rollback "Failed insertion"
                        end
            end
        end
    end

    def add_upvote_comment(_args, _info) do
        {:error, "Not Authorized"}
    end

    def remove_upvote_comment(%{id: comment_id}, %{context: %{current_user: %{id: id}}}) do
        result_success = %{ "result": "Success" }
        query = from c in Comment,
                where: c.id == ^comment_id
        query2 = from ucv in UserCommentVote,
                where: ucv.user_id == ^id 
                    and ucv.comment_id == ^comment_id 
        Repo.transaction fn ->
            case Repo.update_all(query, inc: [upvotes: -1]) do
                nil ->
                    Repo.rollback "Comment not found"
                test -> 
                    query2
                    |> Repo.delete_all
                    |> case do
                        {0,_} -> Repo.rollback "There is no upvote to delete"
                        {1, _} -> result_success
                        {:error, _} ->
                            Repo.rollback "Failed insertion"
                        end
            end
        end
    end

    def remove_upvote_comment(_args, _info) do
        {:error, "Not Authorized"}
    end

    def add_downvote_comment(%{id: comment_id}, %{context: %{current_user: %{id: id}}}) do
        args = Map.merge(%{comment_id: comment_id, user_id: id},%{vote: "downvote"})

        query = from c in Comment,
                where: c.id == ^comment_id
        Repo.transaction fn ->
            case Repo.update_all(query, inc: [downvotes: 1]) do
                nil ->
                    Repo.rollback "Comment not found"
                _ -> 
                    %UserCommentVote{}
                    |> UserCommentVote.changeset(args)
                    |> Repo.insert()
                    |> case do
                        {:ok, result} -> result
                        {:error, _} ->
                            Repo.rollback "Failed insertion"
                        end
            end
        end
    end

    def add_downvote_comment(_args, _info) do
        {:error, "Not Authorized"}
    end

    def remove_downvote_comment(%{id: comment_id}, %{context: %{current_user: %{id: id}}}) do
        result_success = %{ "result": "Success" }
        query = from c in Comment,
                where: c.id == ^comment_id
        query2 = from ucv in UserCommentVote,
                where: ucv.user_id == ^id 
                    and ucv.comment_id == ^comment_id 
        Repo.transaction fn ->
            case Repo.update_all(query, inc: [downvotes: -1]) do
                nil ->
                    Repo.rollback "Comment not found"
                test -> 
                    query2
                    |> Repo.delete_all
                    |> case do
                        {0,_} -> Repo.rollback "There is no downvote to delete"
                        {1, _} -> result_success
                        {:error, _} ->
                            Repo.rollback "Failed insertion"
                        end
            end
        end
    end

    def remove_downvote_comment(_args, _info) do
        {:error, "Not Authorized"}
    end

    def create(args,  %{context: %{current_user: %{id: id}}}) do
        args = Map.merge(args,%{user_id: id})
        %Comment{}
        |> Comment.changeset(args)
        |> Repo.insert
    end

    def createMedia(args,  %{context: %{current_user: %{id: id}}}) do
        args = Map.merge(args,%{user_id: id})
        %Media{}
        |> Media.changeset(args)
        |> Repo.insert
    end   
    
    ## Careful with the updates it is very possible that we need preloads
    def update(%{id: id, comment: comment_params}, _info) do
        Repo.get!(Comment, id)
        |> Comment.changeset(comment_params)
        |> Repo.update
    end

    def delete(%{id: id}, _info) do
        comment = Repo.get!(Comment, id)
        Repo.delete(comment)
    end

end