require IEx

defmodule BackendWeb.Resolvers.EVResolver do
    alias Backend.Repo
    alias BackendWeb.Schema.EV
    alias BackendWeb.Schema.User
    alias BackendWeb.Schema.UserEv
    import Ecto.Query, only: [from: 2]
    
    def create(args, _info) do
        %EV{}
        |> EV.changeset(args)
        |> Repo.insert
    end

    def all_evs(_args,_info) do
        t = {:brand, :model, :avatar}
        case Repo.all(EV) do
            nil -> {:error, "error"}
            evs -> {:ok, evs}
        end
    end

    def add_ev_to_user(%{id: car_id, stats: stats_params}, %{context: %{current_user: %{id: id}}}) do
        args = %{ "consumption": stats_params.consumption, 
            "savings": stats_params.savings, 
            "name": stats_params.name,
            "user_id": id, 
            "ev_id": car_id
        }

        evs = %UserEv{}
        |> UserEv.changeset(args)
        |> Repo.insert
        
        evs
    end

    def add_ev_to_user(_args, _info) do
        {:error, "Not Authorized"}
    end

    def get_evs_from_user(_args, %{context: %{current_user: %{id: id}}}) do
        query = from ue in UserEv,
                where: ue.user_id == ^id,
                preload: [:user]
        user_ev_stats = Repo.all(query)

        {:ok, user_ev_stats}
    end

    def get_evs_from_user(_args, _info) do
        {:error, "Not Authorized"}
    end

    def get_stats_from_ev(%{id: ev_id}, %{context: %{current_user: %{id: id}}}) do
        query = from ue in UserEv,
                where: ue.ev_id == ^ev_id,
                preload: [:ev]
        user_ev_stats = Repo.all(query)

        {:ok, user_ev_stats}
    end

    def get_stats_from_ev(_args, _info) do
        {:error, "Not Authorized"}
    end

    ## Careful with the updates it is very possible that we need preloads
    def update(%{id: id, ev: ev_params}, _info) do
        Repo.get!(EV, id)
        |> EV.changeset(ev_params)
        |> Repo.update
    end

    def delete(%{id: id}, _info) do
        ev = Repo.get!(EV, id)
        Repo.delete(ev)
    end
end