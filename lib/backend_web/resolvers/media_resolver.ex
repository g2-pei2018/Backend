defmodule BackendWeb.Resolvers.MediaResolver do
    alias Backend.Repo
    alias BackendWeb.Schema.Media
    
    def create(args, _info) do
        %Media{}
        |> Media.changeset(args)
        |> Repo.insert
    end

    ## Careful with the updates it is very possible that we need preloads
    def update(%{id: id, media: media_params}, _info) do
        Repo.get!(Media, id)
        |> Media.changeset(media_params)
        |> Repo.update
    end

    def delete(%{id: id}, _info) do
        ev = Repo.get!(EV, id)
        Repo.delete(ev)
    end
end