require IEx

defmodule BackendWeb.Resolvers.PostResolver do
    alias Backend.Repo
    alias BackendWeb.Schema.Post
    alias BackendWeb.Schema.Media
    alias BackendWeb.Schema.UserPostVote
    # Add at top of module
    import Ecto.Query, only: [where: 2, from: 2]
    

    def user_has_voted_post(%{id: post_id}, %{context: %{current_user: %{id: id}}}) do

        query=from upv in UserPostVote, where: upv.user_id == ^id and upv.post_id == ^post_id, select: upv.vote

        case Repo.all(query) do
            nil -> {:error, "User has not voted"}
            votes -> {:ok, votes}
        end
    end

    def user_has_voted_post(_args, _info) do
        {:error, "Not Authorized"}
    end

    def search_posts(%{wildcard: wildcard}, %{context: %{current_user: %{id: id}}}) do

        query=from p in Post,  where: like(p.title, ^wildcard) or like(p.body, ^wildcard), select: p

        case Repo.all(query) do
            nil -> {:error, "Posts not found"}
            posts -> {:ok, posts}
        end
    end

    def search_posts(_args, _info) do
        {:error, "Not Authorized"}
    end


    def all_posts(_args, _info) do
        query = from p in Post,
                order_by: [desc: p.updated_at],
                preload: [:comments]

        case Repo.all(query) do
            nil -> {:error, "Posts not found"}
            posts -> {:ok, posts}
        end
    end

    def posts_from_current_user(_args, %{context: %{current_user: %{id: id}}}) do
        posts =
            Post
            |> where(user_id: ^id)
            |> Repo.all
        case posts do
            nil -> {:error, "Posts for User id #{id} not found"}
            posts -> {:ok, posts}
        end   
    end
    
    def posts_from_current_user(_args, _info) do
        {:error, "Not Authorized"}
    end

    def posts_from_user(%{id: user_id}, %{context: %{current_user: %{id: id}}}) do
        posts =
            Post
            |> where(user_id: ^user_id)
            |> Repo.all
        case posts do
            nil -> {:error, "Posts for User id #{id} not found"}
            posts -> {:ok, posts}
        end   
    end
    
    def posts_from_user(_args, _info) do
        {:error, "Not Authorized"}
    end

    def get_post_by_id(%{id: post_id}, %{context: %{current_user: %{id: id}}}) do
        case Repo.get(Post, post_id) do
            nil -> {:error, "Post id #{id} not found"}
            post -> {:ok, post}
        end
    end

    def get_post_by_id(_args, _info) do
        {:error, "Not Authorized"}
    end

    def get_post_upvotes(%{id: post_id}, _info) do
        case Repo.get(Post, post_id) do
            nil -> {:error, "Post not found"}
            post -> {:ok, post}
        end
    end
    
    def get_post_downvotes(%{id: post_id}, _info) do
        case Repo.get(Post, post_id) do
            nil -> {:error, "Post not found"}
            post -> {:ok, post}
        end
    end

    def add_downvote_post(%{id: post_id}, %{context: %{current_user: %{id: id}}}) do
        args = Map.merge(%{post_id: post_id, user_id: id},%{vote: "downvote"})

        query = from p in Post,
                where: p.id == ^post_id
        Repo.transaction fn ->
            case Repo.update_all(query, inc: [downvotes: 1]) do
                nil ->
                    Repo.rollback "Post not found"
                _ -> 
                    %UserPostVote{}
                    |> UserPostVote.changeset(args)
                    |> Repo.insert()
                    |> case do
                        {:ok, result} -> result
                        {:error, _} ->
                            Repo.rollback "Failed insertion"
                        end
            end
        end
    end

    def add_downvote_post(_args, _info) do
        {:error, "Not Authorized"}
    end

    def remove_downvote_post(%{id: post_id}, %{context: %{current_user: %{id: id}}}) do
        result_success = %{ "result": "Success" }
        query = from p in Post,
                where: p.id == ^post_id
        query2 = from upv in UserPostVote,
                where: upv.user_id == ^id 
                    and upv.post_id == ^post_id 
        Repo.transaction fn ->
            case Repo.update_all(query, inc: [downvotes: -1]) do
                nil ->
                    Repo.rollback "Post not found"
                test -> 
                    query2
                    |> Repo.delete_all
                    |> case do
                        {0,_} -> Repo.rollback "There is no downvote to delete"
                        {1, _} -> result_success
                        {:error, _} ->
                            Repo.rollback "Failed insertion"
                        end
            end
        end
    end

    def remove_downvote_post(_args, _info) do
        {:error, "Not Authorized"}
    end

    def add_upvote_post(%{id: post_id}, %{context: %{current_user: %{id: id}}}) do
        args = Map.merge(%{post_id: post_id, user_id: id},%{vote: "upvote"})

        query = from p in Post,
                where: p.id == ^post_id
        Repo.transaction fn ->
            case Repo.update_all(query, inc: [upvotes: 1]) do
                nil ->
                    Repo.rollback "Post not found"
                _ -> 
                    %UserPostVote{}
                    |> UserPostVote.changeset(args)
                    |> Repo.insert()
                    |> case do
                        {:ok, result} -> result
                        {:error, _} ->
                            Repo.rollback "Failed insertion"
                        end
            end
        end
    end

    def add_upvote_post(_args, _info) do
        {:error, "Not Authorized"}
    end

    def remove_upvote_post(%{id: post_id}, %{context: %{current_user: %{id: id}}}) do
        result_success = %{ "result": "Success" }
        query = from p in Post,
                where: p.id == ^post_id
        query2 = from upv in UserPostVote,
                where: upv.user_id == ^id 
                    and upv.post_id == ^post_id 
        Repo.transaction fn ->
            case Repo.update_all(query, inc: [upvotes: -1]) do
                nil ->
                    Repo.rollback "Post not found"
                test -> 
                    query2
                    |> Repo.delete_all
                    |> case do
                        {0,_} -> Repo.rollback "There is no upvote to delete"
                        {1, _} -> result_success
                        {:error, _} ->
                            Repo.rollback "Failed insertion"
                        end
            end
        end
    end

    def remove_upvote_post(_args, _info) do
        {:error, "Not Authorized"}
    end

    def recent_posts(_args, _info) do
        query = from p in Post,
                limit: 20,
                order_by: [desc: p.updated_at],
                preload: [:comments]
                        
        case Repo.all(query) do
            nil -> {:error, "Posts not found"}
            posts -> {:ok, posts}
        end
    end

    def get_user_avatar(%{id: user_id}, _info) do
        case Repo.get(User, user_id) do
            nil -> {:error, "User not found"}
            user -> {:ok, user.avatar}
        end
    end

    # SELECT *
	# FROM public.posts
    # ORDER BY updated_at DESC 
    # OFFSET offset LIMIT quantity;

    def posts_ordered_desc(%{offset: offset, quantity: quantity}, _info) do
        query = from p in Post,
                offset: ^offset,
                limit: ^quantity,
                order_by: [desc: p.updated_at],
                preload: [:comments]
                        
        case Repo.all(query) do
            nil -> {:error, "Posts not found"}
            posts -> {:ok, posts}
        end
    end

    def posts_ordered_asc(%{offset: offset, quantity: quantity}, _info) do
        query = from p in Post,
                offset: ^offset,
                limit: ^quantity,
                order_by: [asc: p.updated_at],
                preload: [:comments]
                        
        case Repo.all(query) do
            nil -> {:error, "Posts not found"}
            posts -> {:ok, posts}
        end
    end
    
    def create(args,  %{context: %{current_user: %{id: id}}}) do
        #IEx.pry
        args = Map.merge(args,%{user_id: id})
        %Post{}
        |> Post.changeset(args)
        |> Repo.insert
    end

    def createMedia(args,  %{context: %{current_user: %{id: id}}}) do
        args = Map.merge(args,%{user_id: id})
        %Media{}
        |> Media.changeset(args)
        |> Repo.insert
    end   

    def update(%{id: id, post: post_params}, _info) do
        Repo.get!(Post, id)
        |> Post.changeset(post_params)
        |> Repo.update
    end

    def delete(%{id: id}, _info) do
        post = Repo.get!(Post, id)
        Repo.delete(post)
    end

end