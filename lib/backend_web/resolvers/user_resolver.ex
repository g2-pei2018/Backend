require IEx

defmodule BackendWeb.Resolvers.UserResolver do
    alias Backend.Repo
    alias BackendWeb.Schema.User
    alias BackendWeb.Schema.UserEv

    def all_users(_args,_info) do
        case Repo.all(User) do
            nil -> {:error, "Users not found"}
            users -> {:ok, users}
        end
    end

    def get_current_user(_args, %{context: %{current_user: %{id: id}}}) do
        case Repo.get(User, id) do
            nil -> {:error, "User id #{id} not found"}
            user -> {:ok, user}
        end
    end
    
    def get_current_user(_args, _info) do
        {:error, "Not Authorized"}
    end

    def get_user_by_id(%{id: user_id}, %{context: %{current_user: %{id: id}}}) do
        case Repo.get(User, user_id) do
            nil -> {:error, "User id #{id} not found"}
            user -> {:ok, user}
        end
    end

    def get_user_by_id(_args, _info) do
        {:error, "Not Authorized"}
    end

    def update(%{user: user_params}, %{context: %{current_user: %{id: id}}}) do
        user_params = for {key, value} <- user_params, !is_nil(value), into: %{}, do: {key, value}
        Repo.get!(User, id)
        |> User.update_changeset(user_params)
        |> Repo.update
    end

    def update(_args, _info) do
        {:error, "Not Authorized"}
    end

    def login(params, _info) do
        with {:ok, user} <- BackendWeb.Schema.Session.authenticate(params, Repo),
            {:ok, jwt, _ } <- Guardian.encode_and_sign(user, :access) do
            {:ok, %{token: jwt}}
        end
    end

    def signup(args, info) do
        %User{}
        |> User.registration_changeset(args)
        |> Repo.insert
        |> case do
            {:ok, _} -> login(args, info) # We here use info to implement registration and login in a breadth
            {:error, changeset} -> format_changeset(changeset)
           end
           
    end

    def format_changeset(changeset) do
        #{:error, [email: {"has already been taken", []}]}
        errors = changeset.errors
          |> Enum.map(fn({key, {value, context}}) -> 
               [message: "#{key}: #{value}", details: context]
             end)
        {:error, errors}
    end

    def delete(%{id: id}, _info) do
        user = Repo.get!(User, id)
        Repo.delete(user)
    end

    def delete_user_ev(%{id: user_ev_id}, %{context: %{current_user: %{id: id}}}) do
        user_ev = Repo.get!(UserEv,user_ev_id)
        #IEx.pry
        if(user_ev.user_id == id) do
            Repo.delete(user_ev)
        else
        {:error, "The user does not match the car owner"}
        end
    end

end