defmodule BackendWeb.Router do
  use BackendWeb, :router

  pipeline :graphql do
    plug Guardian.Plug.VerifyHeader, realm: "Bearer"
    plug Guardian.Plug.LoadResource
    plug BackendWeb.Plugs.Context
  end


  scope "/api" do
    pipe_through :graphql
    
    forward "/", Absinthe.Plug, schema: BackendWeb.Schema
  end
 
  forward "/graphiql", Absinthe.Plug.GraphiQL,
  schema: BackendWeb.Schema

end
