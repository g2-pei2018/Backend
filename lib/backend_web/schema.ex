defmodule BackendWeb.Schema do
    use Absinthe.Schema
    import_types BackendWeb.Schema.Types
    import_types Absinthe.Type.Custom


    query do
        ############################################################################################################
        # Users
        ############################################################################################################
        field :all_users, list_of(:user) do
            resolve &BackendWeb.Resolvers.UserResolver.all_users/2
        end
        field :get_current_user, type: :user do
            resolve &BackendWeb.Resolvers.UserResolver.get_current_user/2
        end
        field :get_user_by_id, type: :user do
            arg :id, non_null(:id)
            resolve &BackendWeb.Resolvers.UserResolver.get_user_by_id/2
        end
        ############################################################################################################
        # Posts
        ############################################################################################################
        field :all_posts, list_of(:post) do
            resolve &BackendWeb.Resolvers.PostResolver.all_posts/2
        end
        field :get_current_user_posts, list_of(:post) do
            resolve &BackendWeb.Resolvers.PostResolver.posts_from_current_user/2
        end
        field :get_user_posts, type: list_of(:post) do
            arg :id, non_null(:id)
            resolve &BackendWeb.Resolvers.PostResolver.posts_from_user/2
        end
        field :get_post_by_id, type: :post do
            arg :id, non_null(:id)
            resolve &BackendWeb.Resolvers.PostResolver.get_post_by_id/2
        end

        #########################################
        ##### Get Post Number of Upvotes ########
        #########################################
        field :get_post_upvotes, type: :postUpvote do
            arg :id, non_null(:id)
            resolve &BackendWeb.Resolvers.PostResolver.get_post_upvotes/2
        end
        #########################################
        ##### Get Post Number of Downvotes ######
        #########################################
        field :get_post_downvotes, type: :postDownvote do
            arg :id, non_null(:id)
            resolve &BackendWeb.Resolvers.PostResolver.get_post_downvotes/2
        end

        ####################################################
        ####### Posts Ordered DESC By Last Update Date #####
        ####################################################
        field :posts_ordered_desc, list_of(:post) do
            arg :offset, non_null(:integer)
            arg :quantity, non_null(:integer)
            resolve &BackendWeb.Resolvers.PostResolver.posts_ordered_desc/2
        end

        ####################################################
        ####### Posts Ordered ASC By Last Update Date #####
        ####################################################
        field :posts_ordered_asc, list_of(:post) do
            arg :offset, non_null(:integer)
            arg :quantity, non_null(:integer)
            resolve &BackendWeb.Resolvers.PostResolver.posts_ordered_asc/2
        end

        ############################
        ####### Recent Activity ####
        ############################
        field :recent_posts, type: list_of(:post) do
            resolve &BackendWeb.Resolvers.PostResolver.recent_posts/2
        end

        ############################################################################################################
        # Comments
        ############################################################################################################
        field :all_comments, list_of(:comment) do
            resolve &BackendWeb.Resolvers.CommentResolver.all_comments/2
        end

        field :get_current_user_comments, type: list_of(:comment) do
            resolve &BackendWeb.Resolvers.CommentResolver.comments_from_current_user/2
        end
        field :get_user_comments, type: list_of(:comment) do
            arg :id, non_null(:id)
            resolve &BackendWeb.Resolvers.CommentResolver.comments_from_user/2
        end
        ##############################
        ##### List Post Comments #####
        ##############################
        field :get_comment_by_id, type: :comment do
            arg :id, non_null(:id)
            resolve &BackendWeb.Resolvers.CommentResolver.get_comment_by_id/2
        end

        #########################################
        ##### Get Comment Number of Upvotes #####
        #########################################
        field :get_comment_upvotes, type: :commentUpvote do
            arg :id, non_null(:id)
            resolve &BackendWeb.Resolvers.CommentResolver.get_comment_upvotes/2
        end
        #########################################
        ##### Get Comment Number of Downvotes ###
        #########################################
        field :get_comment_downvotes, type: :commentDownvote do
            arg :id, non_null(:id)
            resolve &BackendWeb.Resolvers.CommentResolver.get_comment_downvotes/2
        end


        ############################################################################################################
        # EVs
        ############################################################################################################
        ##############################
        ##### Get User Evs       #####
        ##############################
        field :get_evs_from_user, type: list_of(:user_ev) do
            resolve &BackendWeb.Resolvers.EVResolver.get_evs_from_user/2
        end
        ##############################
        ##### Get Ev statistics  #####
        ##############################
        field :get_stats_from_ev, type: list_of(:user_ev) do
            arg :id, non_null(:id)
            resolve &BackendWeb.Resolvers.EVResolver.get_stats_from_ev/2
        end
        #############################
        ####### List all evs #######
        ############################
        field :list_cars, type: list_of(:list_ev) do
            resolve &BackendWeb.Resolvers.EVResolver.all_evs/2
        end


    
    end

    # input_object :update_comment_params do
    #     field :comment, non_null(:string)
    #     field :upvotes, :integer
    #     field :downvotes, :integer
    #     field :user_id, non_null(:integer)
    # end

    input_object :update_ev_params do
        field :brand, :string
        field :charging_standard, :string
        field :max_charge, :integer
        field :model, :string
        field :year, :integer
        field :avatar, :string

        field :weight, :integer
        field :power, :integer
        field :max_speed, :integer
        field :aerodynamic_coeficient, :float
        field :friction_coefficient, :float
        field :battery, :float
        field :decharge, :float
        field :charge, :float
        field :moteur, :float
        field :precup, :float
        field :category, :string

        field :user_id, non_null(:integer)
    end

    input_object :update_post_params do
        field :title, non_null(:string)
        field :body, non_null(:string)
        field :upvotes, :integer
        field :downvotes, :integer
        field :user_id, non_null(:integer)
    end

    ## Diferentiate between different types of user updates

    input_object :update_user_params do
        field :name, :string
        field :username, :string
        field :email, :string
        field :password, :string
        field :avatar, :string
    end

    # input_object :create_ev_params do
    #     field :brand, non_null(:string)
    #     field :charging_standard, :string
    #     field :max_charge, :integer
    #     field :model, :string
    #     field :year, :integer
    # end

    input_object :create_statistic_params do
        field :consumption, :float
        field :savings, :float
        field :name, :string
    end

    mutation do

        ############################################################################################################
        # Comments
        ############################################################################################################
        
        field :create_comment, type: :comment do
             arg :comment, non_null(:string)
             arg :upvotes, :integer
             arg :downvotes, :integer
             arg :post_id, non_null(:integer)
        
             resolve &BackendWeb.Resolvers.CommentResolver.create/2
        end

        field :create_comment_media, type: :media do
            arg :media, non_null(:string)
            arg :comment_id, non_null(:integer)
        
            resolve &BackendWeb.Resolvers.CommentResolver.createMedia/2
        end

        field :create_post_media, type: :media do
            arg :media, non_null(:string)
            arg :post_id, non_null(:integer)
        
            resolve &BackendWeb.Resolvers.PostResolver.createMedia/2
        end

        # field :update_comment, type: :comment do
        #     arg :id, non_null(:integer)
        #     arg :post, :update_comment_params
        
        #     resolve &BackendWeb.Resolvers.CommentResolver.update/2
        # end

        # field :delete_comment, type: :comment do
        #     arg :id, non_null(:integer)
        
        #     resolve &BackendWeb.Resolvers.CommentResolver.delete/2
        # end


        field :search_comments, list_of(:comment) do
            arg :wildcard, non_null(:string)
            resolve &BackendWeb.Resolvers.CommentResolver.search_comments/2
        end

        ###################################
        ##### Add Upvote to a Comment #####
        ###################################
        field :add_upvote_comment, type: :user_comment_vote do
            arg :id, non_null(:integer)
            resolve &BackendWeb.Resolvers.CommentResolver.add_upvote_comment/2
        end
        ######################################
        ##### Remove Upvote to a Comment #####
        ######################################
        field :remove_upvote_comment, type: :user_comment_vote_remove do
            arg :id, non_null(:integer)
            resolve &BackendWeb.Resolvers.CommentResolver.remove_upvote_comment/2
        end
        #####################################
        ##### Add Downvote to a Comment #####
        #####################################
        field :add_downvote_comment, type: :user_comment_vote do
            arg :id, non_null(:integer)
            resolve &BackendWeb.Resolvers.CommentResolver.add_downvote_comment/2
        end
        ########################################
        ##### Remove Downvote to a Comment #####
        ########################################
        field :remove_downvote_comment, type: :user_comment_vote_remove do
            arg :id, non_null(:integer)
            resolve &BackendWeb.Resolvers.CommentResolver.remove_downvote_comment/2
        end

        #############################
        ####### User Comment Votes ##
        #############################
        field :user_has_voted_comment, type: :integer do
            arg :id, non_null(:integer)
            resolve &BackendWeb.Resolvers.CommentResolver.user_has_voted_comment/2
        end

        ############################################################################################################
        # Posts
        ############################################################################################################
            
        #############################
        ####### User Post Votes #####
        #############################
        field :user_has_voted_post, type: :integer do
            arg :id, non_null(:integer)
            resolve &BackendWeb.Resolvers.PostResolver.user_has_voted_post/2
        end

        ###################################
        ##### Add Upvote to a Post ########
        ###################################
        field :add_upvote_post, type: :user_post_vote do
            arg :id, non_null(:integer)
            resolve &BackendWeb.Resolvers.PostResolver.add_upvote_post/2
        end
        ######################################
        ##### Remove Upvote to a Post #####
        ######################################
        field :remove_upvote_post, type: :user_post_vote_remove do
            arg :id, non_null(:integer)
            resolve &BackendWeb.Resolvers.PostResolver.remove_upvote_post/2
        end
        #####################################
        ##### Add Downvote to a Post #####
        #####################################
        field :add_downvote_post, type: :user_post_vote do
            arg :id, non_null(:integer)
            resolve &BackendWeb.Resolvers.PostResolver.add_downvote_post/2
        end
        ########################################
        ##### Remove Downvote to a Post #####
        ########################################
        field :remove_downvote_post, type: :user_post_vote_remove do
            arg :id, non_null(:integer)
            resolve &BackendWeb.Resolvers.PostResolver.remove_downvote_post/2
        end

        field :get_post_comments, type: list_of(:comment) do
            arg :id, non_null(:id)
            resolve &BackendWeb.Resolvers.CommentResolver.comments_from_post/2
        end
        ##############################################
        ##### Get Number of Comments of one Post #####
        ##############################################
        field :num_comments_from_post, type: :numberComments do
            arg :id, non_null(:id)
            resolve &BackendWeb.Resolvers.CommentResolver.num_comments_from_post/2
        end


        field :create_post, type: :post do
            arg :title, non_null(:string)
            arg :body, non_null(:string)
            arg :upvotes, :integer
            arg :downvotes, :integer
        
            resolve &BackendWeb.Resolvers.PostResolver.create/2
        end

        field :update_post, type: :post do
            arg :id, non_null(:integer)
            arg :post, :update_post_params
        
            resolve &BackendWeb.Resolvers.PostResolver.update/2
        end

        field :delete_post, type: :post do
            arg :id, non_null(:integer)
        
            resolve &BackendWeb.Resolvers.PostResolver.delete/2
        end

        field :search_posts, list_of(:post) do
            arg :wildcard, non_null(:string)
            resolve &BackendWeb.Resolvers.PostResolver.search_posts/2
        end

        ############################################################################################################
        # UsersEVs
        ############################################################################################################
        ##############################
        ##### Add EV To User    #####
        ##############################
        field :add_ev_to_user, type: :user_ev do
            arg :id, non_null(:id)
            arg :stats, :create_statistic_params
        
            resolve &BackendWeb.Resolvers.EVResolver.add_ev_to_user/2
        end

        field :delete_ev_to_user, type: :user_ev do
            arg :id, non_null(:integer)
        
            resolve &BackendWeb.Resolvers.UserResolver.delete_user_ev/2
        end

        ############################################################################################################
        # EVs
        ############################################################################################################
        
        # field :create_ev, type: :ev do
        #     arg :comment, non_null(:string)
        #     arg :upvotes, :integer
        #     arg :downvotes, :integer
        #     arg :user_id, non_null(:integer)
        
        #     resolve &BackendWeb.Resolvers.EVResolver.create/2
        # end
        
        ##############################
        ##### Change Ev Info     #####
        ##############################

        field :update_ev, type: :ev do
            arg :id, non_null(:id)
            arg :ev, :update_ev_params
        
            resolve &BackendWeb.Resolvers.EVResolver.update/2
        end

        # field :delete_ev, type: :ev do
        #     arg :id, non_null(:integer)
        
        #     resolve &BackendWeb.Resolvers.EVResolver.delete/2
        # end

        ############################################################################################################
        # Users
        ############################################################################################################
        
        field :signup, type: :session do 
            arg :name, non_null(:string)
            arg :username, non_null(:string)
            arg :email, non_null(:string)
            arg :password, non_null(:string)
            arg :avatar, :string
        
            resolve &BackendWeb.Resolvers.UserResolver.signup/2
        end

        field :update_user, type: :user do
            arg :user, :update_user_params
        
            resolve &BackendWeb.Resolvers.UserResolver.update/2
        end

        #delete_user
        #field :delete_user, type: :user do
        #    arg :id, non_null(:integer)
        #
        #    resolve &BackendWeb.Resolvers.UserResolver.delete/2
        #end

        field :login, type: :session do
            arg :email, non_null(:string)
            arg :password, non_null(:string)
        
            resolve &BackendWeb.Resolvers.UserResolver.login/2
        end
    end

end