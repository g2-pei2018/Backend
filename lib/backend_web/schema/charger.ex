defmodule BackendWeb.Schema.Charger do
  use Ecto.Schema
  import Ecto.Changeset
  alias BackendWeb.Schema.Charger


  schema "chargers" do
    field :available, :integer
    field :home, :integer
    field :max_current, :integer
    field :plug, :string
    field :power, :integer
    field :power_supply, :string
    field :standard, :string
    field :voltage, :integer
    many_to_many :places, BackendWeb.Schema.Place, join_through: "place_has_charger"
    many_to_many :routes, BackendWeb.Schema.Route, join_through: "route_used_charger"    
    timestamps()
  end

  @doc false
  def changeset(%Charger{} = charger, attrs) do
    charger
    |> cast(attrs, [:standard, :power_supply, :power, :voltage, :max_current, :plug, :home, :available])
    |> validate_required([:standard, :power_supply, :power, :voltage, :max_current, :plug, :home, :available])
  end
end
