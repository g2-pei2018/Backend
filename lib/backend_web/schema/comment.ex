defmodule BackendWeb.Schema.Comment do
  use Ecto.Schema
  import Ecto.Changeset
  alias BackendWeb.Schema.Comment


  schema "comments" do
    field :comment, :string
    field :upvotes, :integer
    field :downvotes, :integer
    belongs_to :post,BackendWeb.Schema.Post
    belongs_to :user,BackendWeb.Schema.User
    has_many :medias, BackendWeb.Schema.Media

    many_to_many :users, BackendWeb.Schema.User, join_through: BackendWeb.Schema.UserCommentVote

    timestamps()
  end

  @doc false
  def changeset(%Comment{} = comment, attrs) do
    comment
    |> cast(attrs, [:comment, :post_id, :user_id])
    |> validate_required([:comment, :post_id, :user_id])
  end
end
