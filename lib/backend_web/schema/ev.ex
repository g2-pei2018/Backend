defmodule BackendWeb.Schema.EV do
  use Ecto.Schema
  import Ecto.Changeset
  alias BackendWeb.Schema.EV


  schema "evs" do
    field :brand, :string
    field :charging_standard, :string
    field :max_charge, :integer
    field :model, :string
    field :year, :integer
    field :avatar, :string
    
    field :weight, :integer
    field :power, :integer
    field :max_speed, :integer
    field :aerodynamic_coeficient, :float
    field :friction_coefficient, :float
    field :battery, :float
    field :decharge, :float
    field :charge, :float
    field :moteur, :float
    field :precup, :float
    field :category, :string

    many_to_many :users, BackendWeb.Schema.User, join_through: BackendWeb.Schema.UserEv
    timestamps()
  end

  @doc false
  def changeset(%EV{} = ev, attrs) do
    ev
    |> cast(attrs, [:brand, :model, :year, :charging_standard, :max_charge])
    |> validate_required([:brand, :model, :year, :charging_standard, :max_charge])
    |> unique_constraint(:model)
  end
end