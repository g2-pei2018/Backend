defmodule BackendWeb.Schema.Media do
  use Ecto.Schema
  import Ecto.Changeset
  alias BackendWeb.Schema.Media


  schema "medias" do
    field :media, :string
    belongs_to :post,BackendWeb.Schema.Post
    belongs_to :user,BackendWeb.Schema.User
    belongs_to :comment,BackendWeb.Schema.Comment

    timestamps()
  end

  @doc false
  def changeset(%Media{} = media, attrs) do
    media
    |> cast(attrs, [:media, :post_id, :comment_id, :user_id])
    |> validate_required([:media, :user_id])
  end
end
