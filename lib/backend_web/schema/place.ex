defmodule BackendWeb.Schema.Place do
  use Ecto.Schema
  import Ecto.Changeset
  alias BackendWeb.Schema.Place


  schema "places" do
    field :country, :string
    field :latitude, :float
    field :longitude, :float
    field :street_name, :string
    field :zip_code, :string
    many_to_many :chargers, BackendWeb.Schema.Charger, join_through: "place_has_charger"
    many_to_many :routes, BackendWeb.Schema.Route, join_through: "route_has_place"
    timestamps()
  end

  @doc false
  def changeset(%Place{} = place, attrs) do
    place
    |> cast(attrs, [:latitude, :longitude, :street_name, :zip_code, :country])
    |> validate_required([:latitude, :longitude, :street_name, :zip_code, :country])
  end
end
