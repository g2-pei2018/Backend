defmodule BackendWeb.Schema.Post do
  use Ecto.Schema
  import Ecto.Changeset
  alias BackendWeb.Schema.Post


  schema "posts" do
    field :body, :string
    field :title, :string
    field :upvotes, :integer
    field :downvotes, :integer
    belongs_to :user,BackendWeb.Schema.User
    has_many :comments, BackendWeb.Schema.Comment
    has_many :medias, BackendWeb.Schema.Media
    
    many_to_many :users, BackendWeb.Schema.User, join_through: BackendWeb.Schema.UserPostVote

    timestamps()
  end

  @doc false
  def changeset(%Post{} = post, attrs) do
    post
    |> cast(attrs, [:title, :body, :upvotes, :downvotes, :user_id])
    |> validate_required([:title, :body, :upvotes, :downvotes, :user_id])
  end
end
