defmodule BackendWeb.Schema.Route do
  use Ecto.Schema
  import Ecto.Changeset
  alias BackendWeb.Schema.Route


  schema "routes" do
    field :nr_p_c_n, :integer
    field :nr_p_c_r, :integer
    many_to_many :places, BackendWeb.Schema.Place, join_through: "route_has_place"    
    many_to_many :chargers, BackendWeb.Schema.Charger, join_through: "route_used_charger"    
    many_to_many :users, BackendWeb.Schema.User, join_through: "users_has_routes"    
    timestamps()
  end

  @doc false
  def changeset(%Route{} = route, attrs) do
    route
    |> cast(attrs, [:nr_p_c_n, :nr_p_c_r])
    |> validate_required([:nr_p_c_n, :nr_p_c_r])
  end
end
