defmodule BackendWeb.Schema.Session do
  alias BackendWeb.Schema.User

  def authenticate(params, repo) do
    case repo.get_by(User, email: String.downcase(params.email)) do
        nil -> {:error, "User not found!"}
        _ -> 
              user = repo.get_by(User, email: String.downcase(params.email))
              match = Hasher.check_password_hash(params.password, user.password_hash)

              case match do
                true -> {:ok, user}
                _ -> {:error, "Incorrect login credentials"}
              end
    end
  end
end