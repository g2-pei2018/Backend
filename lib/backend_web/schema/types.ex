defmodule BackendWeb.Schema.Types do
    use Absinthe.Schema.Notation
    use Absinthe.Ecto, repo: Backend.Repo

    object :user do
        field :id, :id
        field :name, :string
        field :username, :string
        field :avatar, :string
        field :email, :string
        field :posts, list_of(:post), resolve: assoc(:posts)
        field :comments, list_of(:comment), resolve: assoc(:comments)
        field :user_medias, list_of(:media), resolve: assoc(:medias)
        field :user_routes, list_of(:route), resolve: assoc(:routes)
        field :user_evs, list_of(:ev), resolve: assoc(:evs)
        field :inserted_at, :naive_datetime
        field :updated_at, :naive_datetime
    end

    object :route do
        field :nrPCN, :integer
        field :nrPCR, :integer
        field :route_places,list_of(:place), resolve: assoc(:places)
        field :route_chargers,list_of(:charger), resolve: assoc(:chargers)
        field :route_users,list_of(:user), resolve: assoc(:users)
    end

    object :post do
        field :id, :id
        field :title, :string
        field :body, :string
        field :upvotes, :integer
        field :downvotes, :integer
        field :inserted_at, :naive_datetime
        field :updated_at, :naive_datetime
        field :user, :user, resolve: assoc(:user)
        field :comments, list_of(:comment), resolve: assoc(:comments)
        field :medias, list_of(:media), resolve: assoc(:medias)
    end

    object :comment do
        field :id, :id
        field :comment, :string
        field :upvotes, :integer
        field :downvotes, :integer
        field :inserted_at, :naive_datetime
        field :updated_at, :naive_datetime
        field :user, :user, resolve: assoc(:user)
        field :post, :post, resolve: assoc(:post)
        field :medias, list_of(:media), resolve: assoc(:medias)
    end

    object :commentUpvote do
        field :upvotes, :integer
    end

    object :commentDownvote do
        field :downvotes, :integer
    end

    object :postUpvote do
        field :upvotes, :integer
    end

    object :postDownvote do
        field :downvotes, :integer
    end

    object :numberComments do
        field :number, :integer
    end

    object :media do
        field :id, :id
        field :media, :string
        field :user, :user, resolve: assoc(:user)
        field :post, :post, resolve: assoc(:post)
        field :comment, :comment, resolve: assoc(:comment)
    end

    object :place do
        field :country, :string
        field :latitude, :float
        field :longitude, :float
        field :street_name, :string
        field :zip_code, :string
        field :place_chargers,list_of(:charger), resolve: assoc(:chargers)
        field :place_routes,list_of(:route), resolve: assoc(:routes)
    end

    object :ev do
        field :brand, :string
        field :charging_standard, :string
        field :max_charge, :integer
        field :model, :string
        field :year, :integer
        field :avatar, :string

        field :weight, :integer
        field :power, :integer
        field :max_speed, :integer
        field :aerodynamic_coeficient, :float
        field :friction_coefficient, :float
        field :battery, :float
        field :decharge, :float
        field :charge, :float
        field :moteur, :float
        field :precup, :float
        field :category, :string

        field :ev_users,list_of(:user), resolve: assoc(:users)
    end

    object :list_ev do

        field :id, :id
        field :brand, :string
        field :charging_standard, :string
        field :max_charge, :integer
        field :model, :string
        field :year, :integer
        field :avatar, :string

        field :weight, :integer
        field :power, :integer
        field :max_speed, :integer
        field :aerodynamic_coeficient, :float
        field :friction_coefficient, :float
        field :battery, :float
        field :decharge, :float
        field :charge, :float
        field :moteur, :float
        field :precup, :float
        field :category, :string
    end

    object :user_ev do
        field :id, :id
        field :consumption, :float
        field :savings, :float
        field :name, :string
        field :user, :user, resolve: assoc(:user)
        field :ev, :ev, resolve: assoc(:ev)
    end

    object :user_comment_vote do
        field :user, :user, resolve: assoc(:user)
        field :comment, :comment, resolve: assoc(:comment)
    end

    object :user_comment_vote_remove do
        field :result, :string
    end

    object :user_post_vote do
        field :user, :user, resolve: assoc(:user)
        field :post, :post, resolve: assoc(:post)
    end

    object :user_post_vote_remove do
        field :result, :string
    end


    object :charger do
        field :available, :integer
        field :home, :integer
        field :max_current, :integer
        field :plug, :string
        field :power, :integer
        field :power_supply, :string
        field :standard, :string
        field :voltage, :integer
        field :charger_places, list_of(:place), resolve: assoc(:places)
        field :charger_routes, list_of(:route), resolve: assoc(:routes)
    end

    object :session do
        field :token, :string
    end
end