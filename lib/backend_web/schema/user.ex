defmodule BackendWeb.Schema.User do
  use Ecto.Schema
  import Ecto.Changeset
  import Ecto.Query

  schema "users" do
    field :email, :string
    field :name, :string
    field :username, :string
    field :avatar, :string, default: "https://res.cloudinary.com/wattcharger/image/upload/v1517418590/dwnixwo8wg03zcelwpgq.png"
    field :password, :string, virtual: true
    field :password_hash, :string
    #has_many :posts, BackendWeb.Schema.Post
    #has_many :comments, BackendWeb.Schema.Comment
    has_many :medias, BackendWeb.Schema.Media
    many_to_many :routes, BackendWeb.Schema.Route, join_through: "user_has_route"

    field :evs_list, :string, virtual: true
    many_to_many :evs, BackendWeb.Schema.EV, join_through: BackendWeb.Schema.UserEv, on_delete: :delete_all, on_replace: :delete

    field :comments_votes_list, :string, virtual: true
    many_to_many :comments, BackendWeb.Schema.Comment, join_through: BackendWeb.Schema.UserCommentVote, on_delete: :delete_all, on_replace: :delete

    field :posts_votes_list, :string, virtual: true
    many_to_many :posts, BackendWeb.Schema.Post, join_through: BackendWeb.Schema.UserPostVote, on_delete: :delete_all, on_replace: :delete

    timestamps()
  end

  def update_changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:name, :email, :password, :avatar])
    |> validate_format(:email, ~r/@/)
    |> unique_constraint(:email)
    |> validate_length(:password, min: 4)
    |> put_pass_hash()
  end
 
  def registration_changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:name, :username, :email, :password, :avatar, :evs_list, :comments_votes_list, :posts_votes_list])
    |> validate_required([:name, :username, :email, :password])
    |> validate_format(:email, ~r/@/)
    |> unique_constraint(:email)
    |> unique_constraint(:username)
    |> validate_length(:password, min: 4)
    |> put_pass_hash()
  end

  defp error_pass(changeset) do
      password = get_change(changeset, :password)
      if password == "foobar" do
          changeset
      else
          changeset
          |> add_error(:foo, "bar")
      end
  end
 
  defp put_pass_hash(changeset) do
    case changeset do
      %Ecto.Changeset{valid?: true, changes: %{password: pass}} ->
        put_change(changeset, :password_hash, Hasher.salted_password_hash(pass))
      _ ->
        changeset
    end
  end

  def evs(struct, params \\ %{}) do
    struct
    |> cast(params, [:evs_list])
  end
  
  def sorted_by_name do
    from u in BackendWeb.Schema.User,
    order_by: u.name,
    select: u
  end

  def get_by_letter(letter) do
    from u in BackendWeb.Schema.User,
    where: ilike(u.name, ^"#{letter}%"),
    select: u
  end

end
