defmodule BackendWeb.Schema.UserCommentVote do
    use Ecto.Schema
    import Ecto.Changeset
    alias BackendWeb.Schema.UserCommentVote
  
    schema "users_comments_votes" do

      belongs_to :user, BackendWeb.Schema.User
      belongs_to :comment, BackendWeb.Schema.Comment
      field :vote, :string

      timestamps()
    end
  
    def changeset(struct, params \\ %{}) do
      struct
      |> cast(params, [:user_id, :comment_id, :vote])
      |> validate_required([:user_id, :comment_id, :vote])
      |> unique_constraint(:user_id, name: :users_comments_votes_user_id_comment_id_index)
      |> unique_constraint(:comment_id, name: :users_comments_votes_user_id_comment_id_index)
    end
end
  