defmodule BackendWeb.Schema.UserEv do
    use Ecto.Schema
    import Ecto.Changeset
    alias BackendWeb.Schema.UserEv
  
    schema "users_evs" do
      field :consumption, :float
      field :savings, :float
      field :name, :string
  
      belongs_to :user, BackendWeb.Schema.User
      belongs_to :ev, BackendWeb.Schema.EV

      timestamps()
    end
  
    def changeset(struct, params \\ %{}) do
      struct
      |> cast(params, [:user_id, :ev_id, :consumption, :savings, :name])
      |> validate_required([:user_id, :ev_id])
    end
end
  