defmodule BackendWeb.Schema.UserPostVote do
    use Ecto.Schema
    import Ecto.Changeset
    alias BackendWeb.Schema.UserPostVote
  
    schema "users_posts_votes" do

      belongs_to :user, BackendWeb.Schema.User
      belongs_to :post, BackendWeb.Schema.Post
      field :vote, :string

      timestamps()
    end
  
    def changeset(struct, params \\ %{}) do
      struct
      |> cast(params, [:user_id, :post_id, :vote])
      |> validate_required([:user_id, :post_id, :vote])
      |> unique_constraint(:user_id, name: :users_posts_votes_user_id_post_id_index)
      |> unique_constraint(:post_id, name: :users_posts_votes_user_id_post_id_index)
    end
end
  