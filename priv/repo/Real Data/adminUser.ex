# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Backend.Repo.insert!(%Backend.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

alias BackendWeb.Schema.User
alias Backend.Repo

Repo.insert!(%User{name: "Ryan Swapp", email: "ryan@ryan.com", username: "ryan", password_hash: "OHawiA6UB12DfKjG3Iq0/d3GVQm4ppBSMeYgkovAl9I=$LEEq+JKpXMvKN8lL8+jkzQxnyEKV+9bfyiwkOBUVtOs=", avatar: "https://res.cloudinary.com/wattcharger/image/upload/v1516659192/om7hilewv1ksxtyb7fpd.png"})
