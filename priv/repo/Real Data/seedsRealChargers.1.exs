# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Repo.insert!(%SomeModel{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.
alias BackendWeb.Schema.Charger
alias BackendWeb.Schema.Place
# alias BackendWeb.Schema.ChargerPlaces
alias Backend.Repo

# Chargers
charger1 = %Charger{ available: 1, home: 0, max_current: 0, plug: "Mennekes", power: 0, power_supply: "AC", standard: "CSS", voltage: 0 }
charger2 = %Charger{ available: 1, home: 0, max_current: 0, plug: "Mennekes", power: 0, power_supply: "AC", standard: "CSS", voltage: 0 }
charger3 = %Charger{ available: 1, home: 0, max_current: 0, plug: "Mennekes", power: 0, power_supply: "AC", standard: "CSS", voltage: 0 }


# Places

place1 = %Place{ country: "Portugal", latitude: 41.5600174, longitude: -8.3971541, street_name: "R. da Universidade, Braga", zip_code: "4710-057" }



# ChargerPlaces
# chargerplaces1 = %ChargerPlaces{ place_id: 1, charger_id: 1 }
# chargerplaces2 = %ChargerPlaces{ place_id: 1, charger_id: 2 }
# chargerplaces3 = %ChargerPlaces{ place_id: 1, charger_id: 3 }

Repo.insert! charger1
Repo.insert! charger2
Repo.insert! charger3

Repo.insert! place1

# Repo.insert! chargerplaces1
# Repo.insert! chargerplaces2
# Repo.insert! chargerplaces3

