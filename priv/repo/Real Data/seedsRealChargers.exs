# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Repo.insert!(%SomeModel{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.
alias BackendWeb.Schema.Charger
alias BackendWeb.Schema.Place
# alias BackendWeb.Schema.ChargerPlaces
alias Backend.Repo

# Chargers
charger1 = %Charger{ available: 1, home: 0, max_current: 0, plug: "2 Mennekes", power: 22, power_supply: "AC", standard: "CSS", voltage: 0 }
charger2 = %Charger{ available: 1, home: 0, max_current: 0, plug: "2 Mennekes", power: 0, power_supply: "DC", standard: "CSS", voltage: 0 }
charger3 = %Charger{ available: 0, home: 0, max_current: 0, plug: "2 Mennekes", power: 22, power_supply: "AC", standard: "CSS", voltage: 0 }
charger4 = %Charger{ available: 0, home: 0, max_current: 0, plug: "2 Mennekes", power: 0, power_supply: "DC", standard: "CSS", voltage: 0 }

# Places

place1 = %Place{ country: "Portugal", latitude: 41.5600174, longitude: -8.3971541, street_name: "R. da Universidade, Braga", zip_code: "4710-057" }

#PCN
place2 = %Place{ country: "Portugal", latitude: 38.720585000000, longitude: -9.146472000000, street_name: "Hotel Tivoli, Av. Liberdade, 9046", zip_code: "1250-050" }
place3 = %Place{ country: "Portugal", latitude: 38.766582000000, longitude: -9.098366000000, street_name: "Tivoli Oriente, Passeio Báltico", zip_code: "1990-083" }
place4 = %Place{ country: "Portugal", latitude: 38.750939391169, longitude: -9.180281460285, street_name: "Alto dos Moinhos, R. João de Freitas Branco", zip_code: "" } 
place5 = %Place{ country: "Portugal", latitude: 38.748943000000, longitude: -9.138053000000, street_name: "Av. Estados Unidos da América", zip_code: "" }
place6 = %Place{ country: "Portugal", latitude: 38.727409201342, longitude: -9.163332581520, street_name: "R. Marquês de Fronteira, nº 110", zip_code: "" }
place7 = %Place{ country: "Portugal", latitude: 38.769271537954, longitude: -9.159438014030, street_name: "Quinta das Conchas,Alameda das Linhas de Torres", zip_code: "" }
place8 = %Place{ country: "Portugal", latitude: 38.749673849624, longitude: -9.149948358536, street_name: "frente ao edifício da CML, Campo Grande, nº 25", zip_code: "" }
place9 = %Place{ country: "Portugal", latitude: 38.733094229648, longitude: -9.147738218307, street_name: "Maternidade Alfredo da Costa, R. Pedro Nunes", zip_code: "" }
place10 = %Place{ country: "Portugal", latitude: 38.741073848381, longitude: -9.146788716316, street_name: "frente ao edifíicio da EMEL, Av. da República", zip_code: "" } 
place11 = %Place{ country: "Portugal", latitude: 38.709652345249, longitude: -9.142432808876, street_name: "R. António Maria Cardoso, nº 74", zip_code: "" }
place12 = %Place{ country: "Portugal", latitude: 38.712837000000, longitude: -9.160042000000, street_name: "Basílica da Estrela, R. João de Deus ", zip_code: "" }
place13 = %Place{ country: "Portugal", latitude: 38.706657206778, longitude: -9.146202653646, street_name: "Mercado da Ribeira, Av. 24 de Julho", zip_code: "" }
place14 = %Place{ country: "Portugal", latitude: 38.780145200728, longitude: -9.093530774117, street_name: "R. Ilha dos Amores", zip_code: "" }

# available
# PCR
place16 = %Place{ country: "Portugal", latitude: 40.0121, longitude: -8.5995806, street_name: "Pombal – Área de serviço Galp", zip_code: "" }
# place17 = %Place{ country: "Portugal", latitude: 40.0121, longitude: -8.5995806, street_name: "Pombal – Área de serviço Galp", zip_code: "" }
place18 = %Place{ country: "Portugal", latitude: 39.12167, longitude: -8.90806, street_name: "Aveiras de cima – Área de serviço Galp", zip_code: "" }
# place19 = %Place{ country: "Portugal", latitude: 39.12167, longitude: -8.90806, street_name: "Aveiras de cima – Área de serviço Galp", zip_code: "" }
place20 = %Place{ country: "Portugal", latitude: 39.3914195, longitude: -8.7082028, street_name: "Área de serviço BP de Santarém (A1), A1, Santarém", zip_code: "" }
# place21 = %Place{ country: "Portugal", latitude: 39.3914195, longitude: -8.7082028, street_name: "Área de serviço BP de Santarém (A1), A1, Santarém", zip_code: "" }
place22 = %Place{ country: "Portugal", latitude: 40.3326398, longitude: -8.5637996, street_name: "Área de Serviço da Mealhada, Lisboa-Porto", zip_code: "" }
place23 = %Place{ country: "Portugal", latitude: 40.7553, longitude: -8.5349, street_name: "Estarreja (Antuã) – Área de Serviço Repsol km 255", zip_code: "" }
# place24 = %Place{ country: "Portugal", latitude: 40.7553, longitude: -8.5349, street_name: "Estarreja (Antuã) – Área de Serviço Repsol km 255", zip_code: "" }
place25 = %Place{ country: "Portugal", latitude: 39.7120418, longitude: -8.7801692, street_name: "Leiria – Área de Serviço Repsol", zip_code: "" }
# place26 = %Place{ country: "Portugal", latitude: 39.7120418, longitude: -8.7801692, street_name: "Leiria – Área de Serviço Repsol", zip_code: "" }

place27 = %Place{ country: "Portugal", latitude: 38.58594, longitude: -8.9996886, street_name: "Palmela – Área de serviço Galp", zip_code: "" }

place28 = %Place{ country: "Portugal", latitude: 38.3702354, longitude: -8.5764039, street_name: "Alcácer do Sal – Área de serviço Galp", zip_code: "" }
# place29 = %Place{ country: "Portugal", latitude: 38.3702354, longitude: -8.5764039, street_name: "Alcácer do Sal – Área de serviço Galp", zip_code: "" }

place30 = %Place{ country: "Portugal", latitude: 37.9244566, longitude: -8.3134095, street_name: "Aljustrel – Área de serviço Galp", zip_code: "" }
# place31 = %Place{ country: "Portugal", latitude: 37.9244566, longitude: -8.3134095, street_name: "Aljustrel – Área de serviço Galp", zip_code: "" }

place32 = %Place{ country: "Portugal", latitude: 38.7166076, longitude: -9.307182, street_name: "Oeiras – Área de serviço Galp", zip_code: "" }

place33 = %Place{ country: "Portugal", latitude: 37.1365672, longitude: -8.1461191, street_name: "Loulé – Área de serviço Galp", zip_code: "" }
# place34 = %Place{ country: "Portugal", latitude: 37.1365672, longitude: -8.1461191, street_name: "Loulé – Área de serviço Galp", zip_code: "" }

place35 = %Place{ country: "Portugal", latitude: 39.486105, longitude: -8.159812, street_name: "Abrantes – Área de Serviço Cepsa", zip_code: "" }
# place36 = %Place{ country: "Portugal", latitude: 39.486105, longitude: -8.159812, street_name: "Abrantes – Área de Serviço Cepsa", zip_code: "" }

place37 = %Place{ country: "Portugal", latitude: 40.1913983, longitude: -7.5049492, street_name: "Fundão – Área de Serviço Cepsa", zip_code: "" }
# place38 = %Place{ country: "Portugal", latitude: 40.1913983, longitude: -7.5049492, street_name: "Fundão – Área de Serviço Cepsa", zip_code: "" }

place39 = %Place{ country: "Portugal", latitude: 40.6423586, longitude: -8.656768, street_name: "Aveiro – Rossio da cidade de Aveiro", zip_code: "" }
place40 = %Place{ country: "Portugal", latitude: 41.5535456, longitude: -8.4305252, street_name: "Braga – Rua Ferraz, no parque para autocarros", zip_code: "" }
place41 = %Place{ country: "Portugal", latitude: 38.6999593, longitude: -9.4273781, street_name: "Cascais – R. Dom Francisco de Avilez", zip_code: "" }
place42 = %Place{ country: "Portugal", latitude: 40.2033131, longitude: -8.4273787, street_name: "Coimbra –  Av. Lousã, Insúa dos Bentos", zip_code: "" }
place43 = %Place{ country: "Portugal", latitude: 38.5704011, longitude: -7.9157675, street_name: "Évora –  Zona circular contígua à muralha da cidade", zip_code: "" }
place44 = %Place{ country: "Portugal", latitude: 41.4466766, longitude: -8.2992165, street_name: "Guimarães – Av, Dr. Alfedo Pimenta", zip_code: "" }
place45 = %Place{ country: "Portugal", latitude: 38.7596558, longitude: -9.1548053, street_name: "Lisboa I – Avenida Marechal Craveiro Lopes (defronte do Hotel Radisson)", zip_code: "" }
place46 = %Place{ country: "Portugal", latitude: 38.7055016, longitude: -9.1745301, street_name: "Lisboa II – Rua João de Oliveira Minguens, em Alcântara", zip_code: "" }
place47 = %Place{ country: "Portugal", latitude: 38.7579856, longitude: -9.1815499, street_name: "Lisboa – Área de serviço da 2ª Circular sentido S/N – Repsol", zip_code: "" }
place48 = %Place{ country: "Portugal", latitude: 38.7884084, longitude: -9.1477593, street_name: "Lisboa – Área de serviço do Eixo Norte/Sul – Prio, Alta de Lisboa", zip_code: "" }
place49 = %Place{ country: "Portugal", latitude: 38.8253658, longitude: -9.1617479, street_name: "Loures – Passeio do Parque da Cidade", zip_code: "" }
place50 = %Place{ country: "Portugal", latitude: 41.1872784, longitude: -8.6935212, street_name: "Matosinhos – R. França Júnior (frente ao Mercado Municipal)", zip_code: "" }
place51 = %Place{ country: "Portugal", latitude: 41.1628704, longitude: -8.629843, street_name: "Porto - Av. da França", zip_code: "" }
place52 = %Place{ country: "Portugal", latitude: 41.1780258, longitude: -8.6485776, street_name: "Porto – Zona industrial Prio", zip_code: "" }
place53 = %Place{ country: "Portugal", latitude: 42.024981, longitude: -8.6410838, street_name: "Valença –  Av.ª do Colégio Português", zip_code: "" }
place54 = %Place{ country: "Portugal", latitude: 41.7032826, longitude: -8.8209572, street_name: "Viana do Castelo –  Av. Capitão Gaspar Castro, estacionamento da Escola Frei Bartolomeu dos Mártires", zip_code: "" }
place55 = %Place{ country: "Portugal", latitude: 41.0994199, longitude: -8.5672955, street_name: "Vila Nova de Gaia –  Avenida Vasco da Gama", zip_code: "" }
place56 = %Place{ country: "Portugal", latitude: 41.1242612, longitude: -8.6070172, street_name: "Vila Nova de Gaia – Avenida da República", zip_code: "" }
place57 = %Place{ country: "Portugal", latitude: 41.2998767, longitude: -7.7509775, street_name: "Vila Real –  Zona de estacionamento à superfície do Terminal Rodoviário", zip_code: "" }
place58 = %Place{ country: "Portugal", latitude: 38.796314, longitude: -9.1171633, street_name: "Sacavém – Lidl", zip_code: "" }
place59 = %Place{ country: "Portugal", latitude: 38.7285091, longitude: -9.3368249, street_name: "Cascais – Abóboda – Lidl", zip_code: "" }
place60 = %Place{ country: "Portugal", latitude: 41.1991822, longitude: -8.6105527, street_name: "Matosinhos – S. Mamede Infesta – Lidl", zip_code: "" }

# installing
place61 = %Place{ country: "Portugal", latitude: 39.1565512, longitude: -9.2276983, street_name: "A8 – Torres Vedras – Área de Serviço Galp – km 49", zip_code: "" }
# place62 = %Place{ country: "Portugal", latitude: 39.1565512, longitude: -9.2276983, street_name: "A8 – Torres Vedras – Área de Serviço Galp – km 49", zip_code: "" }


# under construction
place62 = %Place{ country: "Portugal", latitude: 41.2715518, longitude: -8.5979424, street_name: "A3 – Trofa (Coronado) – Área de Serviço Galp – km 10", zip_code: "" }
place63 = %Place{ country: "Portugal", latitude: 41.6054363, longitude: -8.5710172, street_name: "A3 – Barcelos – Área de Serviço Cepsa – km 56", zip_code: "" }
# place64 = %Place{ country: "Portugal", latitude: 41.6054363, longitude: -8.5710172, street_name: "A3 – Barcelos – Área de Serviço Cepsa – km 56", zip_code: "" }

place65 = %Place{ country: "Portugal", latitude: 41.2332395, longitude: -8.1793629, street_name: "A4 – Penafiel – Área de Serviço Repsol – km 47,5", zip_code: "" }
# place66 = %Place{ country: "Portugal", latitude: 41.2332395, longitude: -8.1793629, street_name: "A4 – Penafiel – Área de Serviço Repsol – km 47,5", zip_code: "" }

place67 = %Place{ country: "Portugal", latitude: 41.3907141, longitude: -8.4800343, street_name: "A7 – Vila Nova de Famalicão (Seide) – Área de Serviço Galp – km 23", zip_code: "" }
# place68 = %Place{ country: "Portugal", latitude: 41.3907141, longitude: -8.4800343, street_name: "A7 – Vila Nova de Famalicão (Seide) – Área de Serviço Galp – km 23", zip_code: "" }

place69 = %Place{ country: "Portugal", latitude: 37.1519609, longitude: -8.6636637, street_name: "A22 – Lagos – Área de Serviço Galp – km 3", zip_code: "" }
# place70 = %Place{ country: "Portugal", latitude: 37.1519609, longitude: -8.6636637, street_name: "A22 – Lagos – Área de Serviço Galp – km 3", zip_code: "" }

place71 = %Place{ country: "Portugal", latitude: 40.6238637, longitude: -7.9335464, street_name: "A25 – Viseu – Área de Serviço Repsol", zip_code: "" }
# place72 = %Place{ country: "Portugal", latitude: 40.6238637, longitude: -7.9335464, street_name: "A25 – Viseu – Área de Serviço Repsol", zip_code: "" }


place73 = %Place{ country: "Portugal", latitude: 38.7705426, longitude: -9.2972392, street_name: "IC19 – Cacém – Área de Serviço BP sentido Sintra-Lisboa", zip_code: "" }

place74 = %Place{ country: "Portugal", latitude: 38.7688651, longitude: -9.2498627, street_name: "Lisboa – Área de serviço Repsol da 2ª Circular sentido N/S", zip_code: "" }


# ChargerPlaces
# chargerplaces1 = %ChargerPlaces{ place_id: 1, charger_id: 1 }
# chargerplaces2 = %ChargerPlaces{ place_id: 1, charger_id: 2 }
# chargerplaces3 = %ChargerPlaces{ place_id: 1, charger_id: 3 }

Repo.insert! charger1
Repo.insert! charger2

Repo.insert! place1
Repo.insert! place2
Repo.insert! place3
Repo.insert! place4
Repo.insert! place5
Repo.insert! place6
Repo.insert! place7
Repo.insert! place8
Repo.insert! place9
Repo.insert! place10
Repo.insert! place11
Repo.insert! place12
Repo.insert! place13
Repo.insert! place14
Repo.insert! place16
# Repo.insert! place17
Repo.insert! place18
# Repo.insert! place19
Repo.insert! place20
# Repo.insert! place21
Repo.insert! place22
Repo.insert! place23
# Repo.insert! place24
Repo.insert! place25
# Repo.insert! place26
Repo.insert! place27
Repo.insert! place28
# Repo.insert! place29
Repo.insert! place30
# Repo.insert! place31
Repo.insert! place32
Repo.insert! place33
# Repo.insert! place34
Repo.insert! place35
# Repo.insert! place36
Repo.insert! place37
# Repo.insert! place38
Repo.insert! place39
Repo.insert! place40
Repo.insert! place41
Repo.insert! place42
Repo.insert! place43
Repo.insert! place44
Repo.insert! place45
Repo.insert! place46
Repo.insert! place47
Repo.insert! place48
Repo.insert! place49
Repo.insert! place50
Repo.insert! place51
Repo.insert! place52
Repo.insert! place53
Repo.insert! place54
Repo.insert! place55
Repo.insert! place56
Repo.insert! place57
Repo.insert! place58
Repo.insert! place59
Repo.insert! place60
Repo.insert! place61
# Repo.insert! place62
Repo.insert! place63
# Repo.insert! place64
Repo.insert! place65
# Repo.insert! place66
Repo.insert! place67
# Repo.insert! place68
Repo.insert! place69
# Repo.insert! place70
Repo.insert! place71
# Repo.insert! place72
Repo.insert! place73
Repo.insert! place74


# Repo.insert! chargerplaces1
# Repo.insert! chargerplaces2
# Repo.insert! chargerplaces3

