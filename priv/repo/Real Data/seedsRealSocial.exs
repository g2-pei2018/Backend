# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Backend.Repo.insert!(%Backend.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

alias BackendWeb.Schema.User
alias BackendWeb.Schema.Post
alias BackendWeb.Schema.Comment
alias BackendWeb.Schema.Media
alias Backend.Repo

user1 = Repo.insert!(%User{name: "Jorge Ribeiro", email: "jorge@gmail.com", username: "jorge", password_hash: "OHawiA6UB12DfKjG3Iq0/d3GVQm4ppBSMeYgkovAl9I=$LEEq+JKpXMvKN8lL8+jkzQxnyEKV+9bfyiwkOBUVtOs=", avatar: "https://res.cloudinary.com/wattcharger/image/upload/v1517418590/dwnixwo8wg03zcelwpgq.png"})
user2 = Repo.insert!(%User{name: "Daniel Sá", email: "daniel@gmail.com", username: "daniel", password_hash: "OHawiA6UB12DfKjG3Iq0/d3GVQm4ppBSMeYgkovAl9I=$LEEq+JKpXMvKN8lL8+jkzQxnyEKV+9bfyiwkOBUVtOs=", avatar: "https://res.cloudinary.com/wattcharger/image/upload/v1517418590/dwnixwo8wg03zcelwpgq.png"})
user3 = Repo.insert!(%User{name: "Gonçalo Assunção", email: "goncalo@gmail.com", username: "gongas", password_hash: "OHawiA6UB12DfKjG3Iq0/d3GVQm4ppBSMeYgkovAl9I=$LEEq+JKpXMvKN8lL8+jkzQxnyEKV+9bfyiwkOBUVtOs=", avatar: "https://res.cloudinary.com/wattcharger/image/upload/v1517418590/dwnixwo8wg03zcelwpgq.png"})
user4 = Repo.insert!(%User{name: "Ricardo Cardoso", email: "ricardo@gmail.com", username: "ricarod", password_hash: "OHawiA6UB12DfKjG3Iq0/d3GVQm4ppBSMeYgkovAl9I=$LEEq+JKpXMvKN8lL8+jkzQxnyEKV+9bfyiwkOBUVtOs=", avatar: "https://res.cloudinary.com/wattcharger/image/upload/v1517418590/dwnixwo8wg03zcelwpgq.png"})
user5 = Repo.insert!(%User{name: "Fernando Gomes", email: "fernando@gmail.com", username: "fernando", password_hash: "OHawiA6UB12DfKjG3Iq0/d3GVQm4ppBSMeYgkovAl9I=$LEEq+JKpXMvKN8lL8+jkzQxnyEKV+9bfyiwkOBUVtOs=", avatar: "https://res.cloudinary.com/wattcharger/image/upload/v1517418590/dwnixwo8wg03zcelwpgq.png"})
user6 = Repo.insert!(%User{name: "Mónica Dias", email: "monica@gmail.com", username: "monica", password_hash: "OHawiA6UB12DfKjG3Iq0/d3GVQm4ppBSMeYgkovAl9I=$LEEq+JKpXMvKN8lL8+jkzQxnyEKV+9bfyiwkOBUVtOs=", avatar: "https://res.cloudinary.com/wattcharger/image/upload/v1517418590/dwnixwo8wg03zcelwpgq.png"})
user7 = Repo.insert!(%User{name: "Alberto Meireles", email: "alberto@gmail.com", username: "berto", password_hash: "OHawiA6UB12DfKjG3Iq0/d3GVQm4ppBSMeYgkovAl9I=$LEEq+JKpXMvKN8lL8+jkzQxnyEKV+9bfyiwkOBUVtOs=", avatar: "https://res.cloudinary.com/wattcharger/image/upload/v1517418590/dwnixwo8wg03zcelwpgq.png"})
user8 = Repo.insert!(%User{name: "Ricardo Barata", email: "barata@gmail.com", username: "barata", password_hash: "OHawiA6UB12DfKjG3Iq0/d3GVQm4ppBSMeYgkovAl9I=$LEEq+JKpXMvKN8lL8+jkzQxnyEKV+9bfyiwkOBUVtOs=", avatar: "https://res.cloudinary.com/wattcharger/image/upload/v1517418590/dwnixwo8wg03zcelwpgq.png"})
user9 = Repo.insert!(%User{name: "João Pacheco", email: "pacheco@gmail.com", username: "pacheco", password_hash: "OHawiA6UB12DfKjG3Iq0/d3GVQm4ppBSMeYgkovAl9I=$LEEq+JKpXMvKN8lL8+jkzQxnyEKV+9bfyiwkOBUVtOs=", avatar: "https://res.cloudinary.com/wattcharger/image/upload/v1517418590/dwnixwo8wg03zcelwpgq.png"})
user10 = Repo.insert!(%User{name: "Pedro Gramaxo", email: "pedro@gmail.com", username: "pedro", password_hash: "OHawiA6UB12DfKjG3Iq0/d3GVQm4ppBSMeYgkovAl9I=$LEEq+JKpXMvKN8lL8+jkzQxnyEKV+9bfyiwkOBUVtOs=", avatar: "https://res.cloudinary.com/wattcharger/image/upload/v1517418590/dwnixwo8wg03zcelwpgq.png"})
user11 = Repo.insert!(%User{name: "Leonel Tavares", email: "leonel@gmail.com", username: "leo", password_hash: "OHawiA6UB12DfKjG3Iq0/d3GVQm4ppBSMeYgkovAl9I=$LEEq+JKpXMvKN8lL8+jkzQxnyEKV+9bfyiwkOBUVtOs=", avatar: "https://res.cloudinary.com/wattcharger/image/upload/v1517418590/dwnixwo8wg03zcelwpgq.png"})
user12 = Repo.insert!(%User{name: "Fernando Rodrigues", email: "rodrigues@gmail.com", username: "rodas", password_hash: "OHawiA6UB12DfKjG3Iq0/d3GVQm4ppBSMeYgkovAl9I=$LEEq+JKpXMvKN8lL8+jkzQxnyEKV+9bfyiwkOBUVtOs=", avatar: "https://res.cloudinary.com/wattcharger/image/upload/v1517418590/dwnixwo8wg03zcelwpgq.png"})
user13 = Repo.insert!(%User{name: "António Valdo", email: "antonio@gmail.com", username: "valdo", password_hash: "OHawiA6UB12DfKjG3Iq0/d3GVQm4ppBSMeYgkovAl9I=$LEEq+JKpXMvKN8lL8+jkzQxnyEKV+9bfyiwkOBUVtOs=", avatar: "https://res.cloudinary.com/wattcharger/image/upload/v1517418590/dwnixwo8wg03zcelwpgq.png"})
user14 = Repo.insert!(%User{name: "Lídia Carvalho", email: "lidia@gmail.com", username: "lidia", password_hash: "OHawiA6UB12DfKjG3Iq0/d3GVQm4ppBSMeYgkovAl9I=$LEEq+JKpXMvKN8lL8+jkzQxnyEKV+9bfyiwkOBUVtOs=", avatar: "https://res.cloudinary.com/wattcharger/image/upload/v1517418590/dwnixwo8wg03zcelwpgq.png"})
user15 = Repo.insert!(%User{name: "Luis Silveira", email: "luis@gmail.com", username: "luis", password_hash: "OHawiA6UB12DfKjG3Iq0/d3GVQm4ppBSMeYgkovAl9I=$LEEq+JKpXMvKN8lL8+jkzQxnyEKV+9bfyiwkOBUVtOs=", avatar: "https://res.cloudinary.com/wattcharger/image/upload/v1517418590/dwnixwo8wg03zcelwpgq.png"})


post1 = Repo.insert!(%Post{
            title: "Estacionamento Ilegal",
            body: "Aqui em Castelo Branco ... já andam a fazer cumprir a lei",
            user_id: user1.id,
            #upvotes: 2,
            #downvotes: 1
        })
BackendWeb.Resolvers.PostResolver.add_upvote_post(%{id: post1.id},%{context: %{current_user: %{id: user2.id}}})
BackendWeb.Resolvers.PostResolver.add_upvote_post(%{id: post1.id},%{context: %{current_user: %{id: user3.id}}})

BackendWeb.Resolvers.PostResolver.add_downvote_post(%{id: post1.id},%{context: %{current_user: %{id: user4.id}}})

media1 =  Repo.insert!(%Media{
            media: "https://goo.gl/Ybpn3o",
            post_id: post1.id,
            user_id: user1.id
        })

comment1 = Repo.insert!(%Comment{
         comment: "Tem que ser! Só assim.",
         post_id: post1.id,
         user_id: user2.id,
         #upvotes: 3,
         #downvotes: 2
    })

BackendWeb.Resolvers.CommentResolver.add_upvote_comment(%{id: comment1.id},%{context: %{current_user: %{id: user3.id}}})
BackendWeb.Resolvers.CommentResolver.add_upvote_comment(%{id: comment1.id},%{context: %{current_user: %{id: user4.id}}})
BackendWeb.Resolvers.CommentResolver.add_upvote_comment(%{id: comment1.id},%{context: %{current_user: %{id: user5.id}}})

BackendWeb.Resolvers.CommentResolver.add_downvote_comment(%{id: comment1.id},%{context: %{current_user: %{id: user6.id}}})
BackendWeb.Resolvers.CommentResolver.add_downvote_comment(%{id: comment1.id},%{context: %{current_user: %{id: user7.id}}})

comment2 = Repo.insert!(%Comment{
         comment: "Mas assim continua a ocupar o lugar. Seria melhor o reboque!",
         post_id: post1.id,
         user_id: user3.id,
         #upvotes: 4,
         #downvotes: 1
    })

BackendWeb.Resolvers.CommentResolver.add_upvote_comment(%{id: comment2.id},%{context: %{current_user: %{id: user4.id}}})
BackendWeb.Resolvers.CommentResolver.add_upvote_comment(%{id: comment2.id},%{context: %{current_user: %{id: user5.id}}})
BackendWeb.Resolvers.CommentResolver.add_upvote_comment(%{id: comment2.id},%{context: %{current_user: %{id: user6.id}}})
BackendWeb.Resolvers.CommentResolver.add_upvote_comment(%{id: comment2.id},%{context: %{current_user: %{id: user7.id}}})

BackendWeb.Resolvers.CommentResolver.add_downvote_comment(%{id: comment1.id},%{context: %{current_user: %{id: user8.id}}})

comment3 = Repo.insert!(%Comment{
         comment: "Não deu para rebocar.... estava numa zona de difícil acesso.",
         post_id: post1.id,
         user_id: user1.id,
         #upvotes: 4,
         #downvotes: 1
    })
comment4 = Repo.insert!(%Comment{
         comment: "Assim sim!",
         post_id: post1.id,
         user_id: user4.id,
         #upvotes: 3,
         #downvotes: 0
    })
comment5 = Repo.insert!(%Comment{
         comment: "Não serve de muito. Reboque é que é: dói mais a quem não respeita e resolve a aflição de quem quer carregar.",
         post_id: post1.id,
         user_id: user5.id,
         #upvotes: 1,
         #downvotes: 2
    }) 
comment6 = Repo.insert!(%Comment{
         comment: "O reboque seria melhor, sem dúvida. Mas pelo menos, ficando assim lacrado o veículo, serve de exemplo a quem lá passa.",
         post_id: post1.id,
         user_id: user4.id,
         #upvotes: 2,
         #downvotes: 1
         
    })
comment7 = Repo.insert!(%Comment{
         comment: "Acho muito bem... é que a gente quer carregar o carro e não tem como...",
         post_id: post1.id,
         user_id: user6.id,
            #upvotes: 5,
            #downvotes: 1
    })

post2 = Repo.insert!(%Post{
            title: "Consumos",
            body: "Em que valores andam os vossos consumos? Pergunto porque fiz reset aos valores e está-me a ser muito difícil baixar dos 16kwh",
            user_id: user10.id,
            #upvotes: 2,
            #downvotes: 4
        })

BackendWeb.Resolvers.PostResolver.add_upvote_post(%{id: post2.id},%{context: %{current_user: %{id: user11.id}}})
BackendWeb.Resolvers.PostResolver.add_upvote_post(%{id: post2.id},%{context: %{current_user: %{id: user12.id}}})

BackendWeb.Resolvers.PostResolver.add_downvote_post(%{id: post2.id},%{context: %{current_user: %{id: user12.id}}})
BackendWeb.Resolvers.PostResolver.add_downvote_post(%{id: post2.id},%{context: %{current_user: %{id: user14.id}}})
BackendWeb.Resolvers.PostResolver.add_downvote_post(%{id: post2.id},%{context: %{current_user: %{id: user15.id}}})
BackendWeb.Resolvers.PostResolver.add_downvote_post(%{id: post2.id},%{context: %{current_user: %{id: user1.id}}})

comment8 = Repo.insert!(%Comment{
         comment: "15,6 agora no inverno, 12,5 +/- no tempo mais ameno.",
         post_id: post2.id,
         user_id: user11.id,
         #upvotes: 2,
         #downvotes: 1
    })

BackendWeb.Resolvers.CommentResolver.add_upvote_comment(%{id: comment8.id},%{context: %{current_user: %{id: user10.id}}})
BackendWeb.Resolvers.CommentResolver.add_upvote_comment(%{id: comment8.id},%{context: %{current_user: %{id: user15.id}}})

BackendWeb.Resolvers.CommentResolver.add_downvote_comment(%{id: comment1.id},%{context: %{current_user: %{id: user3.id}}})

comment9 = Repo.insert!(%Comment{
         comment: "Meu consumo de Janeiro até à data: 435km com 0,13kwh/km... Maioritariamente cidade do Porto!",
         post_id: post2.id,
         user_id: user12.id,
         #upvotes: 4,
         #downvotes: 2
    })

post3 = Repo.insert!(%Post{
            title: "Mobi.e",
            body: "A realidade é triste mas existe",
            user_id: user13.id,
            #upvotes: 3,
            #downvotes: 2
        })

BackendWeb.Resolvers.PostResolver.add_upvote_post(%{id: post3.id},%{context: %{current_user: %{id: user14.id}}})
BackendWeb.Resolvers.PostResolver.add_upvote_post(%{id: post3.id},%{context: %{current_user: %{id: user15.id}}})
BackendWeb.Resolvers.PostResolver.add_upvote_post(%{id: post3.id},%{context: %{current_user: %{id: user1.id}}})

BackendWeb.Resolvers.PostResolver.add_downvote_post(%{id: post3.id},%{context: %{current_user: %{id: user2.id}}})
BackendWeb.Resolvers.PostResolver.add_downvote_post(%{id: post3.id},%{context: %{current_user: %{id: user3.id}}})

media2 =  Repo.insert!(%Media{
            media: "https://goo.gl/QNoKuz",
            post_id: post3.id,
            user_id: user13.id
        })
comment10 = Repo.insert!(%Comment{
         comment: "será que a mobi-e sabe do que se passa?",
         post_id: post3.id,
         user_id: user13.id,
         #upvotes: 1,
         #downvotes: 2
    })
comment11 = Repo.insert!(%Comment{
         comment: "Em Aveiro há n postos de carregamento, mas apenas funcionam uns 3.",
         post_id: post3.id,
         user_id: user14.id,
         #upvotes: 3,
         #downvotes: 5
    })
comment12 = Repo.insert!(%Comment{
         comment: "Sem dúvida... apoiado",
         post_id: post3.id,
         user_id: user15.id,
         #upvotes: 2,
         #downvotes: 5
    })

post4 = Repo.insert!(%Post{
            title: "E.Leclerc Barcelos",
            body: "Só falta uma inspecção e pelo que informaram é uma questão de poucos dias (veremos).",
            user_id: user7.id,
            #upvotes: 3,
            #downvotes: 1
        })

BackendWeb.Resolvers.PostResolver.add_upvote_post(%{id: post4.id},%{context: %{current_user: %{id: user8.id}}})
BackendWeb.Resolvers.PostResolver.add_upvote_post(%{id: post4.id},%{context: %{current_user: %{id: user9.id}}})
BackendWeb.Resolvers.PostResolver.add_upvote_post(%{id: post4.id},%{context: %{current_user: %{id: user10.id}}})

BackendWeb.Resolvers.PostResolver.add_downvote_post(%{id: post4.id},%{context: %{current_user: %{id: user11.id}}})

media3 =  Repo.insert!(%Media{
            media: "https://goo.gl/EN5dUa",
            post_id: post4.id,
            user_id: user7.id
        })
comment13 = Repo.insert!(%Comment{
         comment: "Se estou a ver o posto que é, está há mais de um ano para entrar em funcionamento..",
         post_id: post4.id,
         user_id: user4.id,
         #upvotes: 8,
         #downvotes: 2
    })        

post5 = Repo.insert!(%Post{
            title: "Autonomia Nissan Leaf",
            body: "Primeiro grande susto! Andei 4 km na A23 a 40 km/h na berma com os 4 piscas. Queria ver o limite mas não queria chegar a este ponto!",
            user_id: user3.id,
            #upvotes: 3,
            #downvotes: 5
        })


BackendWeb.Resolvers.PostResolver.add_upvote_post(%{id: post5.id},%{context: %{current_user: %{id: user4.id}}})
BackendWeb.Resolvers.PostResolver.add_upvote_post(%{id: post5.id},%{context: %{current_user: %{id: user5.id}}})
BackendWeb.Resolvers.PostResolver.add_upvote_post(%{id: post5.id},%{context: %{current_user: %{id: user6.id}}})

BackendWeb.Resolvers.PostResolver.add_downvote_post(%{id: post5.id},%{context: %{current_user: %{id: user7.id}}})
BackendWeb.Resolvers.PostResolver.add_downvote_post(%{id: post5.id},%{context: %{current_user: %{id: user8.id}}})
BackendWeb.Resolvers.PostResolver.add_downvote_post(%{id: post5.id},%{context: %{current_user: %{id: user9.id}}})
BackendWeb.Resolvers.PostResolver.add_downvote_post(%{id: post5.id},%{context: %{current_user: %{id: user10.id}}})
BackendWeb.Resolvers.PostResolver.add_downvote_post(%{id: post5.id},%{context: %{current_user: %{id: user11.id}}})


media4 =  Repo.insert!(%Media{
            media: "https://goo.gl/eYKHQG",
            post_id: post5.id,
            user_id: user3.id
        })        
comment14 = Repo.insert!(%Comment{
         comment: "Há kilometros assim...",
         post_id: post5.id,
         user_id: user8.id,
         #upvotes: 4,
         #downvotes: 2
    }) 

BackendWeb.Resolvers.CommentResolver.add_upvote_comment(%{id: comment14.id},%{context: %{current_user: %{id: user11.id}}})
BackendWeb.Resolvers.CommentResolver.add_upvote_comment(%{id: comment14.id},%{context: %{current_user: %{id: user2.id}}})

BackendWeb.Resolvers.CommentResolver.add_downvote_comment(%{id: comment14.id},%{context: %{current_user: %{id: user9.id}}})


post6 = Repo.insert!(%Post{
            title: "Revisões Renault Zoe",
            body: "Há alguém aqui do grupo que já tenha feito o update do BMS na zona do Porto? Em que oficina foi?",
            user_id: user11.id,
            #upvotes: 3,
            #downvotes: 1
        })


BackendWeb.Resolvers.PostResolver.add_upvote_post(%{id: post6.id},%{context: %{current_user: %{id: user12.id}}})
BackendWeb.Resolvers.PostResolver.add_upvote_post(%{id: post6.id},%{context: %{current_user: %{id: user13.id}}})
BackendWeb.Resolvers.PostResolver.add_upvote_post(%{id: post6.id},%{context: %{current_user: %{id: user14.id}}})

BackendWeb.Resolvers.PostResolver.add_downvote_post(%{id: post6.id},%{context: %{current_user: %{id: user15.id}}})

post7 = Repo.insert!(%Post{
            title: "SC Tesla",
            body: "Hoje carreguei em Fátima. Cheguei com 22% e cheguei aos 80% em 35 minutos.",
            user_id: user12.id,
            #upvotes: 9,
            #downvotes: 3
        })
    

BackendWeb.Resolvers.PostResolver.add_upvote_post(%{id: post7.id},%{context: %{current_user: %{id: user13.id}}})
BackendWeb.Resolvers.PostResolver.add_upvote_post(%{id: post7.id},%{context: %{current_user: %{id: user14.id}}})
BackendWeb.Resolvers.PostResolver.add_upvote_post(%{id: post7.id},%{context: %{current_user: %{id: user15.id}}})
BackendWeb.Resolvers.PostResolver.add_upvote_post(%{id: post7.id},%{context: %{current_user: %{id: user1.id}}})
BackendWeb.Resolvers.PostResolver.add_upvote_post(%{id: post7.id},%{context: %{current_user: %{id: user2.id}}})
BackendWeb.Resolvers.PostResolver.add_upvote_post(%{id: post7.id},%{context: %{current_user: %{id: user3.id}}})
BackendWeb.Resolvers.PostResolver.add_upvote_post(%{id: post7.id},%{context: %{current_user: %{id: user4.id}}})
BackendWeb.Resolvers.PostResolver.add_upvote_post(%{id: post7.id},%{context: %{current_user: %{id: user5.id}}})
BackendWeb.Resolvers.PostResolver.add_upvote_post(%{id: post7.id},%{context: %{current_user: %{id: user6.id}}})

BackendWeb.Resolvers.PostResolver.add_downvote_post(%{id: post7.id},%{context: %{current_user: %{id: user7.id}}})
BackendWeb.Resolvers.PostResolver.add_downvote_post(%{id: post7.id},%{context: %{current_user: %{id: user8.id}}})
BackendWeb.Resolvers.PostResolver.add_downvote_post(%{id: post7.id},%{context: %{current_user: %{id: user9.id}}})

media5 =  Repo.insert!(%Media{
            media: "https://goo.gl/riiMJk",
            post_id: post7.id,
            user_id: user12.id
        }) 

post8 = Repo.insert!(%Post{
            title: "MOBILIDADE URBANA SUSTENTÁVEL",
            body: "Mais de 5.000 veículos elétricos equipa frota da DHL",
            user_id: user2.id,
            #upvotes: 7,
            #downvotes: 3
        })

BackendWeb.Resolvers.PostResolver.add_upvote_post(%{id: post8.id},%{context: %{current_user: %{id: user3.id}}})
BackendWeb.Resolvers.PostResolver.add_upvote_post(%{id: post8.id},%{context: %{current_user: %{id: user4.id}}})
BackendWeb.Resolvers.PostResolver.add_upvote_post(%{id: post8.id},%{context: %{current_user: %{id: user5.id}}})
BackendWeb.Resolvers.PostResolver.add_upvote_post(%{id: post8.id},%{context: %{current_user: %{id: user6.id}}})
BackendWeb.Resolvers.PostResolver.add_upvote_post(%{id: post8.id},%{context: %{current_user: %{id: user7.id}}})
BackendWeb.Resolvers.PostResolver.add_upvote_post(%{id: post8.id},%{context: %{current_user: %{id: user8.id}}})
BackendWeb.Resolvers.PostResolver.add_upvote_post(%{id: post8.id},%{context: %{current_user: %{id: user9.id}}})

BackendWeb.Resolvers.PostResolver.add_downvote_post(%{id: post8.id},%{context: %{current_user: %{id: user10.id}}})
BackendWeb.Resolvers.PostResolver.add_downvote_post(%{id: post8.id},%{context: %{current_user: %{id: user11.id}}})
BackendWeb.Resolvers.PostResolver.add_downvote_post(%{id: post8.id},%{context: %{current_user: %{id: user12.id}}})

media5 =  Repo.insert!(%Media{
            media: "",
            post_id: post8.id,
            user_id: user2.id
        }) 

comment15 = Repo.insert!(%Comment{
         comment: "Hyundai anuncia testes de SUV com células de combustível",
         post_id: post8.id,
         user_id: user2.id,
         #upvotes: 4,
         #downvotes: 0
    })

post9 = Repo.insert!(%Post{
            title: "Motas Elétricas",
            body: "Honda PCX Electric com baterias substituíveis. Este ano no Japão.",
            user_id: user9.id,
            #upvotes: 2,
            #downvotes: 1
        })

BackendWeb.Resolvers.PostResolver.add_upvote_post(%{id: post9.id},%{context: %{current_user: %{id: user10.id}}})
BackendWeb.Resolvers.PostResolver.add_upvote_post(%{id: post9.id},%{context: %{current_user: %{id: user11.id}}})

BackendWeb.Resolvers.PostResolver.add_downvote_post(%{id: post9.id},%{context: %{current_user: %{id: user12.id}}})

media6 =  Repo.insert!(%Media{
            media: "https://goo.gl/wsCuUM",
            post_id: post8.id,
            user_id: user9.id
        }) 

post10 = Repo.insert!(%Post{
            title: "Encontro Fãs Tesla",
            body: "Amanhã, dia 30 às 15h, na marina de oeiras vai haver um café para conversar sobre Teslas!",
            user_id: user5.id,
            #upvotes: 3,
            #downvotes: 0
        })

BackendWeb.Resolvers.PostResolver.add_upvote_post(%{id: post10.id},%{context: %{current_user: %{id: user6.id}}})
BackendWeb.Resolvers.PostResolver.add_upvote_post(%{id: post10.id},%{context: %{current_user: %{id: user7.id}}})
BackendWeb.Resolvers.PostResolver.add_upvote_post(%{id: post10.id},%{context: %{current_user: %{id: user8.id}}})
