# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Repo.insert!(%SomeModel{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

alias BackendWeb.Schema.User
alias BackendWeb.Schema.EV
alias BackendWeb.Schema.UserEv
alias Backend.Repo

user1 = Repo.get!(User, 1)
user2 = Repo.get!(User, 2)
user3 = Repo.get!(User, 3)
user4 = Repo.get!(User, 4)
user5 = Repo.get!(User, 5)
user6 = Repo.get!(User, 6)
user7 = Repo.get!(User, 7)
user8 = Repo.get!(User, 8)
user9 = Repo.get!(User, 9)
user10 = Repo.get!(User, 10)
user11 = Repo.get!(User, 11)
user12 = Repo.get!(User, 12)
user13 = Repo.get!(User, 13)
user14 = Repo.get!(User, 14)
user15 = Repo.get!(User, 15)

evA = Repo.get!(EV, 1)
evB = Repo.get!(EV, 2)
evC = Repo.get!(EV, 3)

userev1=Repo.insert! %UserEv{user: user1, ev: evA, consumption: 1.0, savings: 2.0, name: "carro1"}
userev2=Repo.insert! %UserEv{user: user1, ev: evB, consumption: 100.0, savings: 200.0, name: "carro2"}

userev3=Repo.insert! %UserEv{user: user2, ev: evC, consumption: 1.0, savings: 2.0, name: "carro tio"}
userev4=Repo.insert! %UserEv{user: user3, ev: evB, consumption: 100.0, savings: 200.0, name: "carro pai"}
userev5=Repo.insert! %UserEv{user: user4, ev: evA, consumption: 1.0, savings: 2.0, name: "carro avó"}
userev6=Repo.insert! %UserEv{user: user5, ev: evC, consumption: 100.0, savings: 200.0, name: "carro verão"}
userev7=Repo.insert! %UserEv{user: user6, ev: evA, consumption: 1.0, savings: 2.0, name: "carro trabalho"}
userev8=Repo.insert! %UserEv{user: user7, ev: evC, consumption: 100.0, savings: 200.0, name: "carro viagem"}
userev9=Repo.insert! %UserEv{user: user8, ev: evA, consumption: 1.0, savings: 2.0, name: "carro mãe"}
userev10=Repo.insert! %UserEv{user: user9, ev: evC, consumption: 100.0, savings: 200.0, name: "carro avô"}
userev11=Repo.insert! %UserEv{user: user10, ev: evC, consumption: 1.0, savings: 2.0, name: "carro tia"}
userev12=Repo.insert! %UserEv{user: user11, ev: evB, consumption: 100.0, savings: 200.0, name: "carro primo"}
userev13=Repo.insert! %UserEv{user: user12, ev: evC, consumption: 1.0, savings: 2.0, name: "carro prima"}
userev14=Repo.insert! %UserEv{user: user13, ev: evB, consumption: 100.0, savings: 200.0, name: "carro amigo"}
userev15=Repo.insert! %UserEv{user: user14, ev: evC, consumption: 1.0, savings: 2.0, name: "carro amiga"}
userev16=Repo.insert! %UserEv{user: user15, ev: evB, consumption: 100.0, savings: 200.0, name: "carro favorito"}