defmodule Backend.Repo.Migrations.CreatePosts do
  use Ecto.Migration

  def change do
    create table(:posts) do
      add :title, :string, null: false
      add :body, :text, null: false
      add :upvotes, :integer, null: false, default: 0
      add :downvotes, :integer, null: false, default: 0
      add :user_id, references(:users, on_delete: :nothing), null: false

      timestamps()
    end

  end
end
