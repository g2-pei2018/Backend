defmodule Backend.Repo.Migrations.CreateUsersCommentsVotes do
  use Ecto.Migration

  def change do
    create table(:users_comments_votes) do
      add :user_id, references(:users), null: false
      add :comment_id, references(:comments), null: false
      add :vote, :string, null: false

      timestamps()
    end

    create unique_index(:users_comments_votes, [:user_id,:comment_id])

  end
end