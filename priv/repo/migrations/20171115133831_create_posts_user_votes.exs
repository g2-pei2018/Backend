defmodule Backend.Repo.Migrations.CreateUsersPostsVotes do
  use Ecto.Migration

  def change do
    create table(:users_posts_votes) do
      add :user_id, references(:users), null: false
      add :post_id, references(:posts), null: false
      add :vote, :string, null: false

      timestamps()
    end

    create unique_index(:users_posts_votes, [:user_id,:post_id])

  end
end