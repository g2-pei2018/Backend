defmodule Backend.Repo.Migrations.CreateMedias do
  use Ecto.Migration

  def change do
    create table(:medias) do
      add :media, :string, null: false
      add :post_id, references(:posts, on_delete: :nothing)
      add :user_id, references(:users, on_delete: :nothing), null: false
      add :comment_id, references(:comments, on_delete: :nothing)
      
      timestamps()
    end

    create index(:medias, [:post_id, :comment_id, :user_id])
  end
end
