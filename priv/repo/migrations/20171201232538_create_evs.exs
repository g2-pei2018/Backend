defmodule Backend.Repo.Migrations.CreateEvs do
  use Ecto.Migration

  def change do
    create table(:evs) do
      add :brand, :string, null: false
      add :model, :string, null: false
      add :year, :integer, null: false
      add :avatar, :string, null: true
      add :charging_standard, :string, null: false
      add :max_charge, :integer, null: false

      add :weight, :integer, null: false
      add :power, :integer, null: false
      add :max_speed, :integer, null: false
      add :aerodynamic_coeficient, :float, null: false
      add :friction_coefficient, :float, null: false
      add :battery, :float, null: false
      add :decharge, :float, null: false
      add :charge, :float, null: false
      add :moteur, :float, null: false
      add :precup, :float, null: false
      add :category, :string, null: false

      timestamps()
    end

    # create unique_index(:evs, [:model])

  end
end
