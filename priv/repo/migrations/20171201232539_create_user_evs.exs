defmodule Backend.Repo.Migrations.CreateUsersEvs do
  use Ecto.Migration

  def change do
    create table(:users_evs) do
      add :user_id, references(:users), null: false
      add :ev_id, references(:evs), null: false
      add :consumption, :float, null: false, default: 0
      add :savings, :float, null: false, default: 0
      add :name, :string, null: false

      timestamps()
    end

    create unique_index(:users_evs, [:user_id,:ev_id])

  end
end