defmodule Backend.Repo.Migrations.CreateChargers do
  use Ecto.Migration

  def change do
    create table(:chargers) do
      add :standard, :string, null: false
      add :power_supply, :string, null: false
      add :power, :integer, null: false
      add :voltage, :integer, null: false
      add :max_current, :integer, null: false
      add :plug, :string, null: false
      add :home, :integer, null: false, default: 0
      add :available, :integer, null: false, default: 1

      timestamps()
    end

  end
end
