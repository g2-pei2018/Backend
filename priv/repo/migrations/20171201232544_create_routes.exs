defmodule Backend.Repo.Migrations.CreateRoutes do
  use Ecto.Migration

  def change do
    create table(:routes) do
      add :nr_p_c_r, :integer, null: false
      add :nr_p_c_n, :integer, null: false

      timestamps()
    end

  end
end
