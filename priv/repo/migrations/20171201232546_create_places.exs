defmodule Backend.Repo.Migrations.CreatePlaces do
  use Ecto.Migration

  def change do
    create table(:places) do
      add :latitude, :float, null: false
      add :longitude, :float, null: false
      add :street_name, :string, null: false
      add :zip_code, :string, null: false
      add :country, :string, null: false

      timestamps()
    end

  end
end
