defmodule Backend.Repo.Migrations.CreateUserRoute do
    use Ecto.Migration
    
    def change do
        create table(:user_has_route, primary_key: false) do
            add :user_id, references(:users), null: false
            add :route_id, references(:routes), null: false
        end

    end
end