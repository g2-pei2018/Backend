defmodule Backend.Repo.Migrations.CreateRoutePlace do
    use Ecto.Migration
    
    def change do
        create table(:route_has_place, primary_key: false) do
            add :route_id, references(:routes), null: false
            add :place_id, references(:places), null: false
        end

    end
end