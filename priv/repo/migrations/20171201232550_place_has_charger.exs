defmodule Backend.Repo.Migrations.CreatePlaceCharger do
    use Ecto.Migration
    
    def change do
        create table(:place_has_charger, primary_key: false) do
            add :place_id, references(:places), null: false
            add :charger_id, references(:chargers), null: false
        end

    end
end