defmodule Backend.Repo.Migrations.CreateRouteCharger do
    use Ecto.Migration
    
    def change do
        create table(:route_used_charger, primary_key: false) do
            add :route_id, references(:routes), null: false
            add :charger_id, references(:chargers), null: false
        end

    end
end