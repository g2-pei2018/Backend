#description:Opel Ampera-e,
#description after trimming token ' ' becomes brand and model

# brand: "Opel",
# model: "Ampera-e",
# year: 2017,
# max_charge: 30,
# charging_standard: "CCS",
# avatar: "https://www.google.pt/url?sa=i&rct=j&q=&esrc=s&source=images&cd=&cad=rja&uact=8&ved=0ahUKEwiu_sj88sjYAhVHuRQKHRu_CrYQjRwIBw&url=https%3A%2F%2Fhum3d.com%2F3d-models%2Fopel-ampera-e_2017%2F&psig=AOvVaw2ConbKEER_HxoU_N1WBado&ust=1515519129515349",
# weight: 1625,
# power: 150,
# max_speed: 150,
# scx: 0.64,
# cr: 0.015,
# batery: 60,
# decharge: 0.97,
# charge: 0.97,
# moteur: 0.75,
# precup: 0.4,
# category: "Cars"


# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Repo.insert!(%SomeModel{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.
# https://myelectriccar.com.au/renault-zoe/

alias BackendWeb.Schema.EV
alias Backend.Repo

# EVs

leaf = %EV{
    brand: "Nissan", 
    model: "Leaf", 
    year: 2017, 
    max_charge: 30, 
    charging_standard: "CHAdeMO", 
    avatar: "https:#www.google.pt/url?sa=i&rct=j&q=&esrc=s&source=images&cd=&cad=rja&uact=8&ved=0ahUKEwjQivHqusbYAhWKbRQKHSAmBbAQjRwIBw&url=https%3A%2F%2Fwww.nissanusa.com%2Felectric-cars%2Fleaf%2F&psig=AOvVaw1a03BCFKdnPAi1IoyK_545&ust=1515435371208398",
    weight: 1625,
    power: 150,
    max_speed: 150,
    scx: 0.64,
    cr: 0.015,
    batery: 60,
    decharge: 0.97,
    charge: 0.97,
    moteur: 0.75,
    precup: 0.4,
    category: "Cars"
}
zoe = %EV{
    brand: "Renault", 
    model: "Zoe", 
    year: 1917, 
    max_charge: 40, 
    charging_standard: "CCS", 
    avatar: "https:#www.google.pt/url?sa=i&rct=j&q=&esrc=s&source=images&cd=&cad=rja&uact=8&ved=0ahUKEwiHxpmlu8bYAhUM1hQKHT9WC6sQjRwIBw&url=https%3A%2F%2Fmyelectriccar.com.au%2Frenault-zoe%2F&psig=AOvVaw3yWPZAkcKL1_mfGJyjqB-R&ust=1515435493708699",
    weight: 1625,
    power: 150,
    max_speed: 150,
    scx: 0.64,
    cr: 0.015,
    batery: 60,
    decharge: 0.97,
    charge: 0.97,
    moteur: 0.75,
    precup: 0.4,
    category: "Cars"
}

ampera = %EV{
    brand: "Opel",
    model: "Ampera-e",
    year: 2017,
    max_charge: 30,
    charging_standard: "CCS",
    avatar: "https://www.google.pt/url?sa=i&rct=j&q=&esrc=s&source=images&cd=&cad=rja&uact=8&ved=0ahUKEwiu_sj88sjYAhVHuRQKHRu_CrYQjRwIBw&url=https%3A%2F%2Fhum3d.com%2F3d-models%2Fopel-ampera-e_2017%2F&psig=AOvVaw2ConbKEER_HxoU_N1WBado&ust=1515519129515349",
    weight: 1625,
    power: 150,
    max_speed: 150,
    scx: 0.64,
    cr: 0.015,
    batery: 60,
    decharge: 0.97,
    charge: 0.97,
    moteur: 0.75,
    precup: 0.4,
    category: "Cars"
}

Repo.insert! leaf
Repo.insert! zoe
Repo.insert! ampera


