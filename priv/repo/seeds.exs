# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Backend.Repo.insert!(%Backend.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

alias BackendWeb.Schema.User
alias BackendWeb.Schema.Post
alias BackendWeb.Schema.Comment
alias BackendWeb.Schema.Media
alias Backend.Repo

Repo.insert!(%User{name: "Ryan Swapp", email: "ryan@ryan.com", username: "ryan", password_hash: "OHawiA6UB12DfKjG3Iq0/d3GVQm4ppBSMeYgkovAl9I=$LEEq+JKpXMvKN8lL8+jkzQxnyEKV+9bfyiwkOBUVtOs=", avatar: "https://res.cloudinary.com/wattcharger/image/upload/v1516659192/om7hilewv1ksxtyb7fpd.png"})

for user_id <- 1..10 do
Repo.insert!(%User{name: "Tiago O Grande#{user_id}", email: "tiago@dastreet.com#{user_id}", username: "Tiago#{user_id}", password_hash: "OHawiA6UB12DfKjG3Iq0/d3GVQm4ppBSMeYgkovAl9I=$LEEq+JKpXMvKN8lL8+jkzQxnyEKV+9bfyiwkOBUVtOs=", avatar: "https://res.cloudinary.com/wattcharger/image/upload/v1516659192/om7hilewv1ksxtyb7fpd.png"})
Repo.insert!(%User{name: "Bernardo O Pequeno#{user_id}", email: "bermas@dastreet.com#{user_id}", username: "bernas#{user_id}", password_hash: "OHawiA6UB12DfKjG3Iq0/d3GVQm4ppBSMeYgkovAl9I=$LEEq+JKpXMvKN8lL8+jkzQxnyEKV+9bfyiwkOBUVtOs=", avatar: "https://res.cloudinary.com/wattcharger/image/upload/v1516659192/om7hilewv1ksxtyb7fpd.png"})
end 

for user_id <- 1..10 do

    post = Repo.insert!(%Post{
        title: Faker.Lorem.sentence |> String.slice(0..200),
        body: Faker.Lorem.paragraph |> String.slice(0..200),
        user_id: user_id
    })
    BackendWeb.Resolvers.PostResolver.add_upvote_post(%{id: post.id},%{context: %{current_user: %{id: user_id}}})
	#BackendWeb.Resolvers.PostResolver.add_downvote_post(%{id: post.id},%{context: %{current_user: %{id: user_id}}})
	#BackendWeb.Resolvers.PostResolver.remove_upvote_post(%{id: post.id},%{context: %{current_user: %{id: user_id}}})
	#BackendWeb.Resolvers.PostResolver.remove_downvote_post(%{id: post.id},%{context: %{current_user: %{id: user_id}}})
end

for user_id <- 1..10 do
    
    comment = Repo.insert!(%Comment{
         comment: Faker.Lorem.sentence |> String.slice(0..200),
         post_id: [1,2,3,4,5,6,7,8,9,10] |> Enum.take_random(1) |> hd,
         user_id: user_id
    })
    BackendWeb.Resolvers.CommentResolver.add_upvote_comment(%{id: comment.id},%{context: %{current_user: %{id: user_id}}})
	#BackendWeb.Resolvers.CommentResolver.add_downvote_comment(%{id: comment.id},%{context: %{current_user: %{id: user_id}}})
	#BackendWeb.Resolvers.CommentResolver.remove_upvote_comment(%{id: comment.id},%{context: %{current_user: %{id: user_id}}})
	#BackendWeb.Resolvers.CommentResolver.remove_downvote_comment(%{id: comment.id},%{context: %{current_user: %{id: user_id}}})
end

for _ <- 1..10 do
    Repo.insert!(%Media{
         # url partial
         # media: "v1516659192/om7hilewv1ksxtyb7fpd.png",
         media: "https://res.cloudinary.com/wattcharger/image/upload/v1516659192/om7hilewv1ksxtyb7fpd.png",
         post_id: [1,2,3,4,5,6,7,8,9,10] |> Enum.take_random(1) |> hd,
         user_id: [1,2,3] |> Enum.take_random(1) |> hd,
         comment_id: [1,2,3,4,5,6,7,8,9,10] |> Enum.take_random(1) |> hd
    })
end

# Full URL 
# http://res.cloudinary.com/wattcharger/image/upload/v1516659192/om7hilewv1ksxtyb7fpd.png