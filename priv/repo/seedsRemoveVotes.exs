# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Backend.Repo.insert!(%Backend.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

alias BackendWeb.Schema.User
alias BackendWeb.Schema.Post
alias BackendWeb.Schema.Comment
alias BackendWeb.Schema.Media
alias Backend.Repo

for user_id <- 1..10 do

   BackendWeb.Resolvers.PostResolver.remove_upvote_post(%{id: user_id},%{context: %{current_user: %{id: user_id}}})

end

for user_id <- 1..10 do

   BackendWeb.Resolvers.CommentResolver.remove_upvote_comment(%{id: user_id},%{context: %{current_user: %{id: user_id}}})

end