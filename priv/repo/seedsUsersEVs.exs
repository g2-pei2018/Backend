# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Repo.insert!(%SomeModel{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

alias BackendWeb.Schema.User
alias BackendWeb.Schema.EV
alias BackendWeb.Schema.UserEv
alias Backend.Repo

# EVs

zoe = Repo.get!(EV, 2)
zoe1 = Repo.get!(EV, 3)
zoe2 = Repo.get!(EV, 4)
zoe3 = Repo.get!(EV, 17)
zoe4 = Repo.get!(EV, 5)
zoe5 = Repo.get!(EV, 6)
zoe6 = Repo.get!(EV, 7)
zoe7 = Repo.get!(EV, 8)
zoe8 = Repo.get!(EV, 9)
zoe9 = Repo.get!(EV, 10)
zoe10 = Repo.get!(EV, 11)
zoe11 = Repo.get!(EV, 12)
zoe12 = Repo.get!(EV, 13)
zoe13 = Repo.get!(EV, 14)
zoe15 = Repo.get!(EV, 15)
zoe16 = Repo.get!(EV, 16)



# Users

# users = [
#   %User{name: "Ryan Swapp 2", email: "ryan2@ryan.com", username: "ryan2", password_hash: "OHawiA6UB12DfKjG3Iq0/d3GVQm4ppBSMeYgkovAl9I=$LEEq+JKpXMvKN8lL8+jkzQxnyEKV+9bfyiwkOBUVtOs=", avatar: "https://unsplash.com/photos/mEZ3PoFGs_k"},
#   %User{name: "Tiago O Grande 2", email: "tiago2@dastreet.com", username: "Tiago2", password_hash: "OHawiA6UB12DfKjG3Iq0/d3GVQm4ppBSMeYgkovAl9I=$LEEq+JKpXMvKN8lL8+jkzQxnyEKV+9bfyiwkOBUVtOs=", avatar: "https://unsplash.com/photos/oJUTLMxMsgE"},
#   %User{name: "Bernardo O Pequeno 2", email: "bermas2@dastreet.com", username: "bernas2", password_hash: "OHawiA6UB12DfKjG3Iq0/d3GVQm4ppBSMeYgkovAl9I=$LEEq+JKpXMvKN8lL8+jkzQxnyEKV+9bfyiwkOBUVtOs=", avatar: "https://unsplash.com/photos/a7bdqjeG6M4"}  
# ]


# Enum.each(users, fn(user) ->
#   Repo.insert! user
# end)


userA = Repo.get!(User, 1)

Repo.insert! %UserEv{user: userA, ev: zoe1, consumption: 1.0, savings: 2.0, name: "carro1"}
Repo.insert! %UserEv{user: userA, ev: zoe2, consumption: 100.0, savings: 200.0, name: "carro2"}
Repo.insert! %UserEv{user: userA, ev: zoe3, consumption: 100.0, savings: 200.0, name: "carro2"}
Repo.insert! %UserEv{user: userA, ev: zoe4, consumption: 1.0, savings: 2.0, name: "carro1"}
Repo.insert! %UserEv{user: userA, ev: zoe5, consumption: 100.0, savings: 200.0, name: "carro2"}
Repo.insert! %UserEv{user: userA, ev: zoe6, consumption: 100.0, savings: 200.0, name: "carro2"}
Repo.insert! %UserEv{user: userA, ev: zoe7, consumption: 1.0, savings: 2.0, name: "carro1"}
Repo.insert! %UserEv{user: userA, ev: zoe8, consumption: 100.0, savings: 200.0, name: "carro2"}
Repo.insert! %UserEv{user: userA, ev: zoe9, consumption: 100.0, savings: 200.0, name: "carro2"}
Repo.insert! %UserEv{user: userA, ev: zoe10, consumption: 1.0, savings: 2.0, name: "carro1"}
Repo.insert! %UserEv{user: userA, ev: zoe11, consumption: 100.0, savings: 200.0, name: "carro2"}
Repo.insert! %UserEv{user: userA, ev: zoe12, consumption: 100.0, savings: 200.0, name: "carro2"}
Repo.insert! %UserEv{user: userA, ev: zoe13, consumption: 1.0, savings: 2.0, name: "carro1"}
Repo.insert! %UserEv{user: userA, ev: zoe15, consumption: 100.0, savings: 200.0, name: "carro2"}
Repo.insert! %UserEv{user: userA, ev: zoe16, consumption: 1.0, savings: 2.0, name: "carro1"}

#changesetA = Ecto.Changeset.change(userev1)
#Repo.update!(changesetA)
#changesetB = Ecto.Changeset.change(userev2)
#Repo.update!(changesetB)


