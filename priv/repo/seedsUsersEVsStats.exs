# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Repo.insert!(%SomeModel{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

alias BackendWeb.Schema.User
alias BackendWeb.Schema.EV
alias BackendWeb.Schema.UserEv
alias Backend.Repo

# EVs

leaf = Repo.get!(EV, 1)
zoe = Repo.get!(EV, 2)

# User

userA = Repo.get!(User, 4)

userev1 = Repo.get!(UserEv, 1)
userev2 = Repo.get!(UserEv, 2)

changeset1 = UserEv.changeset(userev1, %{consumption: 150.0, savings: 120.0, name: "carro1"})
changeset2 = UserEv.changeset(userev2, %{consumption: 250.0, savings: 320.0, name: "carro2"})

Repo.update(changeset1)
Repo.update(changeset2)